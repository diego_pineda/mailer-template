<?php

if ( ! function_exists('clearData'))
{
    function clearData($table)
    {
		$CI =& get_instance();
		 $field = $CI->db->list_fields($table);
            $obj = new stdClass();
            $data = array();

            if($field){
                foreach ($field as $key => $value) {
                    # code...
                    $obj->$value = '';
                }
            }

             $data[0] = $obj;
           return $data;
	} 
}

if ( ! function_exists('_getXML'))
{
    function _getXML($fname) {
            $CI =& get_instance();
            $xmlfile = $fname;
            $xmlRaw = file_get_contents($xmlfile);

            $CI->load->library('simplexml');
            $xmlData = $CI->simplexml->xml_parse($xmlRaw);


            return $xmlData;
}
}

if ( ! function_exists('impr'))
{
    function impr($data) {
            echo '<pre>';
            print_r($data);
            echo '</pre>';
    }
}



if ( ! function_exists('stringURLSafe'))
{
    function stringURLSafe($string) {
        //remove any '-' from the string they will be used as concatonater
        $str = str_replace('_', ' ', $string);

        // remove any duplicate whitespace, and ensure all characters are alphanumeric
        $str = preg_replace(array('/\s+/', '/[^A-Za-z0-9\-]/'), array('-', ''), $str);

        // lowercase and trim
        $str = trim(strtolower($str));
        return $str;
    }
}


if ( ! function_exists('validateString'))
{
    function validateString($value) {
        $string = $value;
        //remove any '-' from the string they will be used as concatonater
        return (!empty($string))?$string:'';
    }
}


if ( ! function_exists('replacePath'))
{
    function replacePath($value,$folder='') {
        
        $file = str_replace('application/modules','', $value);
        $replace = (!empty($folder))?$folder:'full';
        $file = str_replace('full',$replace, $file);
        return $file;
    }
}





if ( ! function_exists('percent'))
{
    function percent($int,$percent='100') {
        
        //remove any '-' from the string they will be used as concatonater
        return ($int*$percent)/100;
    }
}


if ( ! function_exists('convertDataFiles'))
{
    function convertDataFiles($files){
        $arr = array();
        if($files){
            foreach($files as $key => $value){
                if(!empty($value)){
                    foreach ($value as $k => $v) {
                        if(!empty($v)){
                            foreach ($v as $i => $j) {
                                if(!empty($j) and $k != 'error'){
                                    $arr[$i][$k] = $j;
                                }else if(!empty($arr[$i])){
                                     if(!array_key_exists("error",$arr[$i])){
                                        $arr[$i]['error'] =  $j;
                                    } 
                                }
                               
                            }
                        }
                    }
                }
            }
        }
        return $arr;
    }
}

if ( ! function_exists('select'))
{
    function select($attr, $arr, $default = '', $value = '', $name = '', $extra = '') {
        $defaults = array(
            'name' => $attr['name'],
            'id' => (array_key_exists('id',$attr)) ? $attr['id'] : $attr['name']
                );
                
            $array = array_diff($attr, array(''));
        ?>
        <select <?= _parse_form_attributes($array, $defaults) . $extra ?> >
                <option value="">Selected</option>
        <?
        if (is_array($arr)) {
            foreach ($arr as $k => $v) {
                $val = ($value) ? $v->$value : $k
                ?>
                    <option value="<?= $val ?>" <?= ($val == $default and !empty($default)) ? 'selected="selected"' : '' ?> ><?= ($name) ? $v->$name : $v ?></option>
            <? }
        }
        ?>
        </select>
        <?php
    }
}


if ( ! function_exists('getVideo'))
{
    function getVideo($url) {
        $id = $url;
        $id_video = explode("=", $id);
        $posicion = strrpos($id_video[1], "&");
        if ($posicion === false) {
             $video =$id_video[1];   
         }else{
              $videos = explode("&",$id_video[1]);
              $video = $videos[0];
        }

        $video = '<iframe width="399" height="224" src="http://www.youtube.com/embed/'.$video.'?feature=oembed" frameborder="0" allowfullscreen=""></iframe>';
        return $video;
    }
}

?>