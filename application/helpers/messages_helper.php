<?php

if ( ! function_exists('set_message'))
{
    function set_message($type, $content)
    {
		$CI =& get_instance();
		$temp = $CI->session->userdata('messages');
		if(!is_array($temp)) $temp = array();
		$temp[] = array('type'=>$type, 'content'=>$content);
		$CI->session->set_userdata('messages',$temp);
	}
}

if ( ! function_exists('html_messages'))
{
	function html_messages(){
		$CI =& get_instance();
		$array = $CI->session->userdata('messages');
		$CI->session->set_userdata('messages',array());
		if(!is_array($array))return false;
		$html = '<!-- Alert Boxes - Start -->';
		foreach($array as $alert):
			$html.=
			'<div class="alert alert-'.$alert['type'].' fade in">
				 <button data-dismiss="alert" class="close close-sm" type="button">
                                      <i class="fa fa-times"></i>
				</button>
				'.$alert['content'].'
			</div>';
		endforeach;		
		$html.= '<!-- Alert Boxes - End -->';
		return $html;
	}
}


?>