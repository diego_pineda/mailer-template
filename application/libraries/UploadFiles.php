<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  

require('class.upload.php');

ini_set( 'display_errors', 1 );
error_reporting( E_ALL ^ E_NOTICE );

class UploadFiles {
	
	var $path;
	var $folder;
	
	function __construct() {
		$this->CI =& get_instance();
		$this->folder ='application/modules/'.$this->CI->uri->segment(1).'/assets/upload';
		$this->path = FCPATH.$this->folder;
	}
	function setFolder($folder){
		$this->folder = $folder;

	}

	function resizedImages($file){


		$foo = new Upload($file);
		$random = '-'.rand(0,4);
		$foo->file_name_body_add = $random;
		//$foo->file_overwrite = true;
		$response = array();
		if ($foo->uploaded) {
			  $foo->Process($this->path.'/full');
			  if ($foo->processed) {
			    $response['status'] = 'ok';
			    $response['msg'] = 'Upload succesfull';
			    $response['url'] = $this->folder.'/full/'.$foo->file_src_name_body.$random.'.'.$foo->file_src_name_ext;
			  } else {
			  	$response['status'] = 'error';
			    $response['msg'] =  $foo->error;
			    echo $this->path;
			    print_r($response);
			    exit;
			  }
			  $with = $foo->image_src_x;
			 
			  // resized to 75%
			  $foo->file_new_name_body = $foo->file_src_name_body;
			  $foo->image_resize = true;
			  $foo->image_x =  percent($with,75);
			  $foo->image_ratio_y = true;
			  $foo->file_name_body_add = $random;
			  $foo->Process($this->path.'/medium');

			  // resized to 35%
			  $foo->file_new_name_body = $foo->file_src_name_body;
			  $foo->image_resize = true;
			  $foo->image_x =  percent($with,35);
			  $foo->image_ratio_y = true;
			  $foo->file_name_body_add = $random;
			  $foo->Process($this->path.'/small');
			  
		}
		return $response;
	}

	function upload($file){

		$foo = new Upload($file);
		$foo->file_overwrite = true;
		$response = array();
		if ($foo->uploaded) {
		  // save uploaded image with no changes 
		  $foo->Process($this->path);
		  if ($foo->processed) {
		    $response['status'] = 'ok';
		    $response['msg'] = 'Upload sussefull';
		    $response['url'] = $this->folder.'/'.$foo->file_src_name;
		    $response['file'] = $foo;
		  } else {
		  	$response['status'] = 'error';
		    $response['msg'] =  $foo->error;
		    $response['file'] = $foo;
		  }
		}
		return $response;
	}
	
	
	
	
        
}