<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  



class Installer {
	
	var $file;
	var $folder;
	
	function __construct() {
		$this->CI =& get_instance();
		$this->folder ='application/modules/'.$this->CI->uri->segment(1).'/assets/upload';
		$this->path = FCPATH.$this->folder;
	}

	function setFile($file){
		$this->file = $file;

	} 

	function unzip($url){
		 $zip = new ZipArchive;
         $res = $zip->open($url);
        if ($res === TRUE) {
          $zip->extractTo($this->path);
          $zip->close();
          set_message('success',' unzip succesfull'.' - '.$path);
        } else {
          set_message('danger'," I not open ".$url);
        }
	}

	function readXml(){
		$filename = $this->file->file_dst_name_body.'.xml';
		$source = $this->file->file_dst_path.$this->file->file_dst_name_body.'/';
		$read = $source.'/'.$filename;
		$destination = FCPATH.'application/modules/';
		if(file_exists($read)){
			$data = _getXML($read);
			//$mv = 'mv -v '.$source.$this->file->file_dst_name_body.'  '.$destination.$this->file->file_dst_name_body.' >  '.FCPATH.'logs/cms_installer_log.txt';
			/*echo ' source '.$source;
			echo ' destination '.$destination;
			exit;*/
			$this->copyDirectory($source.$this->file->file_dst_name_body,$destination.$this->file->file_dst_name_body); 
			//exec($mv); 
			set_message('success'," File  ".$mv);
		}else{
			set_message('danger'," File  ".$filename.' Dont exist !');
		}

		/*print_r($read);
		
		exit;*/
	}

	function copyDirectory($sourceDir,$targetDir) { 
       if (!file_exists($sourceDir)) return false;
	    if (!is_dir($sourceDir)) return copy($sourceDir, $targetDir);
	    if (!mkdir($targetDir)) return false;
	    foreach (scandir($sourceDir) as $item) {
	      if ($item == '.' || $item == '..') continue;
	      if (!self::copyDirectory($sourceDir.DIRECTORY_SEPARATOR.$item, $targetDir.DIRECTORY_SEPARATOR.$item)) return false;
	    }
	    return true; 
    } 

	
	
        
}