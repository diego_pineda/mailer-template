
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                CMS Users Admin
            </header>
            <div class="panel-body">
                <div class="adv-table">
                    <a href="<?= base_url('cms_users/edit/'); ?>" class="btn btn-primary"><i class="fa fa-user"></i> Add user</a>
                    <table  class="display table table-bordered table-striped" id="data_table">
                        <thead>
                            <tr>
                                <th class="center hidden-phone" style="width: 30px">Id.</th>
                                <th>Name</th>
                                <th class="hidden-phone">E-mail</th>
                                <th class="hidden-phone">Role</th>
                                <th class="hidden-phone">Client</th>
                                <th class="hidden-phone" style="width: 110px">Last login</th>

                                <th class="center" style="width: 30px">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php foreach($users as $user):  ?>
                            <tr class="">
                                <td class="hidden-phone"><?=$user->pk_user; ?></td>
                                <td><?=$user->name; ?></td>
                                <td><?=$user->email; ?></td>
                                <td class="hidden-phone"><?=$user->role; ?></td>
                                <td class="hidden-phone"><?=$user->client; ?></td>
                                <td class="hidden-phone"><?=$user->last_login; ?></td>
                                <td >
                                    <div class="btn-group">
                                        <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><i class="fa "></i> Actions <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            
                                            <li><a href="<?= base_url('cms_users/edit/'.$user->pk_user); ?>">Edit</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#" class="delete" data-pk="<?= $user->pk_user ?>">Delete</a></li>
                                        </ul>
                                        
                                    </div>
                                </td>
                            </tr>
                            
                            <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="center hidden-phone">Id.</th>
                                <th>name</th>
                                <th>E-mail</th>
                                <th class="hidden-phone">Role</th>
                                <th class="hidden-phone">Client</th>
                                <th class="hidden-phone">Last login</th>
                                <th class="center">Actions</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<script>
     jQuery(function(){
        jQuery('.delete').on('click',function(e){
            e.preventDefault();
            var pk_data = jQuery(this).data('pk');
            var url = "<?= base_url('cms_users/delete/') ?>";
            var btn = jQuery(this);
           jQuery.post(url,{ pk_user: pk_data, table: 'cms_users'},function(data){
                if(data.status == 'ok'){
                    btn.parents('tr').eq(0).remove();
                }
            
            },'json');
        })

    })
</script> 