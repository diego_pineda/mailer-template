<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
               CMS Users Admin - <?php echo ($user === false || count($user)==0) ? "Add User":"Edit User"; ?>
            </header>
            <div>
              <?= html_messages()?> 
            </div>
            <div class="panel-body">
                <br>
                <form id="form-edit" class="form-horizontal form" action="/cms_users/save/" method="post"  enctype="multipart/form-data">
                  <input type="hidden" name='pk_user' value='<?= $data->pk_user?>'>
                  <input type="hidden" name='table' value='cms_users'>
                  <input type="hidden" name='redirect' value='cms_users/edit/'> 
                    <div class="form-group">
                        <label for="name" class="col-lg-2 col-sm-2 control-label">Name</label>
                        <div class="col-lg-10 ">
                            <input type="text" class="form-control validate[required]" id="name" name="name" placeholder="Enter Name" value="<?= $data->name?>">
                            <!--<p class="help-block">Complete Name</p>-->
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-lg-2 col-sm-2 control-label">Email</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="email" id="email" placeholder="Enter Email" value="<?= $data->email?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-lg-2 col-sm-2 control-label">password</label>
                        <div class="col-lg-10">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Enter password"> 
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="fk_role" class="col-lg-2 col-sm-2 control-label">Role</label>
                        <div class="col-lg-10">
                            <?
                                $attr= array('name'=>'fk_role','class'=>'validate[required] form-control');
                                echo select($attr,$roles,$data->fk_role,'pk_role','name');
                            ?>
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="fk_client" class="col-lg-2 col-sm-2 control-label">Client</label>
                        <div class="col-lg-10">
                            <?
                                $attr= array('name'=>'fk_client','class'=>'validate[required] form-control');
                                echo select($attr,$clients,$data->fk_client,'pk_client','name');
                            ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                      <button type="submit" class="btn btn-success">Save</button>
                      <a class="btn btn-info" href="<?= base_url('cms_users')?>">Cancel</a>
                    </div>
                </form>

            </div>
        </section>

    </div>
</div>