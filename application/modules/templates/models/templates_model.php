<?php

/**
 * Administrator_model 
 * 
 * @category Category 
 * @author Diego F. Pineda
 * @email diegopineda@latinmedios.com
 * @agency Latinmedios INC. 
 */
class Templates_model extends CI_Model {

    var $upload_path;

    function __construct() {
        parent::__construct();
        $this->upload_path = realpath(APPPATH . "../application/uploads");
        $this->load->model('../cms/models/administrator_model');
    }

    /**
     * Validate function
     * @param array $param1 data username and Password of person
     * @return Query Object 
     */
    function templateList($aliasLayout = '') {

        if ($aliasLayout != '') {
            $this->db->select('tbl_templates_saved.*');
            $this->db->select('tbl_templates_layouts.alias');
            $this->db->from('tbl_templates_saved');
            $this->db->join('tbl_templates_layouts', 'tbl_templates_layouts.pk_templates_layout = tbl_templates_saved.fk_templates_layout');
            $this->db->where('tbl_templates_saved.fk_status', 1);
            $this->db->where('tbl_templates_layouts.alias', $aliasLayout);
            $query = $this->db->get();

            return $query->result();
        } else {
            return false;
        }
    }

    function layoutList() {

        $this->db->select('*');
        $this->db->from('tbl_templates_layouts');
        $this->db->where('tbl_templates_layouts.fk_status', 1);
        $query = $this->db->get();
        return $query->result();
    }

    function loadLayout($pk_template_layout = '') {
        if ($pk_template_layout) {
            $this->db->select('*');
            $this->db->from('tbl_templates_layouts');
            $this->db->where('tbl_templates_layouts.pk_templates_layout', $pk_template_layout);
            $query = $this->db->get();
            return $query->result();
        } else {
            return false;
        }
    }

    function loadLayoutbyAlias($alias = '') {
        if ($alias) {
            $this->db->select('*');
            $this->db->from('tbl_templates_layouts');
            $this->db->where('tbl_templates_layouts.alias', $alias);
            $query = $this->db->get();
            return $query->result();
        } else {
            return false;
        }
    }

    function loadModules($pk_template_layout = '') {
        if ($pk_template_layout) {
            $this->db->select('*');
            $this->db->from('tbl_templates_modules');
            $this->db->where('tbl_templates_modules.fk_templates_layout', $pk_template_layout);
            $query = $this->db->get();
            return $query->result();
        } else {
            return false;
        }
    }

    function loadModulebyId($pk_templates_module = '') {
        if ($pk_templates_module) {
            $this->db->select('filepath');
            $this->db->from('tbl_templates_modules');
            $this->db->where('tbl_templates_modules.pk_templates_module', $pk_templates_module);
            $query = $this->db->get();
            return $query->result();
        } else {
            return false;
        }
    }

    function loadSaveMail($pk_templates_saved = '') {
        if ($pk_templates_saved) {
            $this->db->select('landingCode');
            $this->db->from('tbl_templates_saved');
            $this->db->where('tbl_templates_saved.pk_templates_saved', $pk_templates_saved);
            $this->db->where('tbl_templates_saved.fk_status', 1);
            $query = $this->db->get();
            return $query->result();
        } else {
            return false;
        }
    }
    
    function rawCode($pk_templates_saved = '') {
        if ($pk_templates_saved) {
            $this->db->select('*'); 
            $this->db->from('tbl_templates_saved');
            $this->db->where('tbl_templates_saved.pk_templates_saved', $pk_templates_saved);
            $this->db->where('tbl_templates_saved.fk_status', 1);
            $query = $this->db->get();
            
            return $query->result();
            
        } else {
            return false;
        }
    }
    
    function loadTemplate($pk_templates_saved = '') {
        if ($pk_templates_saved) {
            $this->db->select('*');
            $this->db->from('tbl_templates_saved');
            $this->db->where('tbl_templates_saved.pk_templates_saved', $pk_templates_saved);
            $query = $this->db->get();
            return $query->result();
        } else {
            return false;
        }
    }

    function deleteRow($pk_templates_saved = '') {
        if ($pk_templates_saved) {

            $data = array('fk_status' => 0);
            $this->db->where('pk_templates_saved', $pk_templates_saved);
            $query = $this->db->update('tbl_templates_saved', $data);
            return $query;
        }
    }

//
//    function cmsConfig() {
//        $this->db->order_by('pk_config', 'asc');
//        $query = $this->db->get('tbl_cms_config');
//        return $query->result();
//    }
    }
    