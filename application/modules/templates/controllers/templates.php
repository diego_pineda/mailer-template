<?php

class templates extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->upload_path = FCPATH . 'application/modules/' . $this->uri->segment(1) . "/assets/uploads/";
        $this->load->model('cms/administrator_model');
    }

    function index() {
        $session_user = $this->session->all_userdata();
        $is_logged = $this->session->userdata('logged_in');
        if ($is_logged === TRUE) {
            $header_data['user'] = $session_user['username'];

            /*
             * Begin load of CMS views and Configuration defaults
             */
            // HTML <HEAD> load config_cms data
            $head['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["head"] = $this->load->view('cms/head', $head, true);
            //HTML BODY <HEADER> Load data
            $header_data['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["header"] = $this->load->view('cms/header', $header_data, true);
            //HTML BODY <ASIDE> Load MainMenu
            $cms_menu = $this->administrator_model->menuIndex();
            $aside_data['cms_menu'] = json_encode($cms_menu);
            $template_data["aside"] = $this->load->view('cms/aside', $aside_data, true);
            //HTML BODY <FOOTER> Load Footer Info
            $template_data["footer"] = $this->load->view('cms/footer', NULL, true);
            //other Configurations
            $template_data['cms_config'] = $this->administrator_model->cmsConfig();
            $moduleData['def_var'] = '';
            /*
             * End of CMS defaults Section.
             */

            /*             * ************************************************************************ */

            /*
             * Here goes all the data section module 
             */

            //$this->load->model('templates_model');
            //$moduleData['users'] = $this->templates_model->usersList();
            $icons = $this->db->get('tbl_cms_icons');
            $moduleData['icons'] = $icons->result();
            //print_r($moduleData['icons']);
            //exit;
            $template_data["mainview"] = $this->load->view('index', $moduleData, true);




            /*
             * End of  data section module 
             */
            /*             * ************************************************************************ */
            //HTML BODY content #main-content Load Info   
            $this->load->view('cms/index', $template_data);
        } else {
            redirect(base_url('cms_login'));
        }
    }

    function view() {
        $session_user = $this->session->all_userdata();
        $is_logged = $this->session->userdata('logged_in');
        if ($is_logged === TRUE) {
            $header_data['user'] = $session_user['username'];

            /*
             * Begin load of CMS views and Configuration defaults
             */
            // HTML <HEAD> load config_cms data
            $head['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["head"] = $this->load->view('cms/head', $head, true);
            //HTML BODY <HEADER> Load data
            $header_data['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["header"] = $this->load->view('cms/header', $header_data, true);
            //HTML BODY <ASIDE> Load MainMenu
            $cms_menu = $this->administrator_model->menuIndex();
            $aside_data['cms_menu'] = json_encode($cms_menu);
            $template_data["aside"] = $this->load->view('cms/aside', $aside_data, true);
            //HTML BODY <FOOTER> Load Footer Info
            $template_data["footer"] = $this->load->view('cms/footer', NULL, true);
            //other Configurations
            $template_data['cms_config'] = $this->administrator_model->cmsConfig();
            $moduleData['def_var'] = '';
            /*
             * End of CMS defaults Section.
             */

            /*             * ************************************************************************ */

            /*
             * Here goes all the data section module 
             */

            $this->load->model('templates_model');
            $moduleData['aliasTemplate'] = $this->uri->segment(3);

            $moduleData['templates'] = $this->templates_model->templateList($moduleData['aliasTemplate']);


            //print_r($moduleData['icons']);
            //exit;
            $template_data["mainview"] = $this->load->view('index', $moduleData, true);




            /*
             * End of  data section module 
             */
            /*             * ************************************************************************ */
            //HTML BODY content #main-content Load Info   
            $this->load->view('cms/index', $template_data);
        } else {
            redirect(base_url('cms_login'));
        }
    }

    function create($type = '') {
        $session_user = $this->session->all_userdata();
        $is_logged = $this->session->userdata('logged_in');
        if ($is_logged === TRUE) {
            $header_data['user'] = $session_user['username'];

            /*
             * Begin load of CMS views and Configuration defaults
             */
            // HTML <HEAD> load config_cms data
            $head['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["head"] = $this->load->view('cms/head', $head, true);
            //HTML BODY <HEADER> Load data
            $header_data['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["header"] = $this->load->view('cms/header', $header_data, true);
            //HTML BODY <ASIDE> Load MainMenu
            $cms_menu = $this->administrator_model->menuIndex();
            $aside_data['cms_menu'] = json_encode($cms_menu);
            $template_data["aside"] = $this->load->view('cms/aside', $aside_data, true);
            //HTML BODY <FOOTER> Load Footer Info
            $template_data["footer"] = $this->load->view('cms/footer', NULL, true);
            //other Configurations
            $template_data['cms_config'] = $this->administrator_model->cmsConfig();
            $moduleData['def_var'] = '';
            /*
             * End of CMS defaults Section.
             */

            /*             * ************************************************************************ */

            /*
             * Here goes all the data section module 
             */
            $templateAlias = $this->uri->segment(3);
            if ($templateAlias != '') {
                if ($templateAlias == 'edit') {
                    $this->load->model('templates_model');
                    $moduleData['SelectedTemplate'] = $this->loadRawCode($this->uri->segment(4));
                    $fk_template = $moduleData['SelectedTemplate']->fk_templates_layout;
                    $SelectedLayout = $this->templates_model->loadLayout($fk_template);

                    $moduleData['templateAlias'] = $SelectedLayout[0]->alias;

                    $moduleData['layouts'] = $this->templates_model->layoutList();
                } else {
                    $this->load->model('templates_model');

                    $moduleData['SelectedLayout'] = $this->templates_model->loadLayoutbyAlias($this->uri->segment(3));
                    $moduleData['layouts'] = $this->templates_model->layoutList();
                    $moduleData['templateAlias'] = $this->uri->segment(3);
                }
            } else {
                redirect(base_url('templates/view'));
            }

            //print_r($moduleData['icons']);
            //exit;
            $template_data["mainview"] = $this->load->view('create', $moduleData, true);




            /*
             * End of  data section module 
             */
            /*             * ************************************************************************ */
            //HTML BODY content #main-content Load Info   
            $this->load->view('cms/index', $template_data);
        } else {
            redirect(base_url('cms_login'));
        }
    }

    function loadTemplateAjax($templates_id) {

        $this->load->model('templates_model');

        $result = $this->templates_model->loadTemplate($templates_id);
        $SelectedLayout = $result[0]->rawCode;
        $layout = $result[0]->fk_templates_layout;
        //exit;
        $data['SelectedLayout'] = $SelectedLayout;
        echo $SelectedLayout;
    }

    function loadLayoutAjax($layout_id) {

        $this->load->model('templates_model');

        $result = $this->templates_model->loadLayout($layout_id);
        $layout = $result[0]->basecode;
        $data['SelectedLayout'] = $layout_id;
        $this->load->view($layout, $data);
    }

    function loadModulesAjax($layout_id) {

        $this->load->model('templates_model');

        $layouts = $this->templates_model->loadModules($layout_id);

        $html = '<select id="modules_loaded" class="form-control">';
        //$html .='<option>Select Module</option>';
        foreach ($layouts as $layout) {
            $html.='<option  value="' . $layout->pk_templates_module . '">' . $layout->name . '</option>';
        }
        $html.='</select>';
        echo $html;
    }

    function loadModulesbyID($module_id) {

        $this->load->model('templates_model');

        $result = $this->templates_model->loadModulebyId($module_id);
        $module = $result[0]->filepath;
        $this->load->view($module);
    }

    function saveImageData() {

        $data = explode(",", $_POST['image_as_base64']);
        //print_r($data);

        $im = base64_decode($data[1]);
        if ($im !== false) {
            //header('Content-Type: image/png');
            //imagepng($im);

            $image = md5(date("Y-m-d H:i:s")) . ".png";
            $imgFile = $this->upload_path . $image;
            $handle = fopen($imgFile, 'w') or die('Cannot open file:  ' . $imgFile);
            $data = $im;
            fwrite($handle, $data);
            echo base_url('templates/assets/uploads/' . $image);
        } else {
            echo 'error';
        }
    }

    function savetemplate() {
        $query = $this->db->query('select max(pk_templates_saved) as max from tbl_templates_saved');
        $query = $query->result();
        $max = ($query[0]->max) + 1;
        $liveLink = base_url('landingpage/' . $max);
        //exit;
        $data = $_POST;

        $data['date_added'] = date('Y-m-d H:i:s');
        $toReplace = array('data-toggle="modal"', 'class="editable-field"', 'contenteditable="true"', 'edit_image');
        $forReplace = array('', '', '', '');

        $data['sourceCode'] = str_replace($toReplace, $forReplace, $data['sourceCode']);


        $data['landingCode'] = str_replace('<span style="display:block;text-align:center; font-family:Arial, Helvetica, sans-serif; font-size:11px; margin: 10px 0; color:#666;">Si no puede ver este E-mail haga clic <a href="{clickAqui}" title="">aquí</a></span>', '', $data['sourceCode']);

        $data['sourceCode'] = str_replace('{clickAqui}', $liveLink, $data['sourceCode']);

        $this->db->insert('tbl_templates_saved', $data);
        echo $data['sourceCode'];
    }

    function savetemplate2($pk_templates_saved = '') {
        
        if ($pk_templates_saved != '') {

            $liveLink = base_url('landingpage/' . $pk_templates_saved);
            //exit;
            
            $data = $_POST;
            
            
            $toReplace = array('data-toggle="modal"', 'class="editable-field"', 'contenteditable="true"', 'edit_image');
            $forReplace = array('', '', '', '');

            $data['sourceCode'] = str_replace($toReplace, $forReplace, $data['sourceCode']);


            $data['landingCode'] = str_replace('<span style="display:block;text-align:center; font-family:Arial, Helvetica, sans-serif; font-size:11px; margin: 10px 0; color:#666;">Si no puede ver este E-mail haga clic <a href="{clickAqui}" title="">aquí</a></span>', '', $data['sourceCode']);

            $data['sourceCode'] = str_replace('{clickAqui}', $liveLink, $data['sourceCode']);

            $this->db->where('pk_templates_saved', $pk_templates_saved);
            $this->db->update('tbl_templates_saved', $data);
            echo $data['sourceCode'];
        }
    }

    function loadtitles($templateAlias='') {
         
        if($templateAlias=='edit' || $templateAlias=='other' ){
            $pk_templates_saved=$this->uri->segment(4);
             $this->load->model('templates_model');
              $result = $this->templates_model->loadTemplate($pk_templates_saved);
              if($result){
                  
                  
                  $layout = $this->templates_model->loadLayout($result[0]->fk_templates_layout);
                  
                  $templateAlias = $layout[0]->alias; 
                  
              }
        }
        echo $templateAlias;
        $dir = FCPATH . 'application/modules/' . $this->uri->segment(1) . '/assets/' . $templateAlias . '/images/titles';

        $titles = array();
        if (is_dir($dir)) {
            if ($gd = opendir($dir)) {
                $no = 0;
                while ($archivo = readdir($gd)) {

                    if ($archivo != '..' && $archivo != '.') {
                        $titles[$no] = $archivo;
                    }
                    $no++;
                }
                closedir($gd);
            }
            array_multisort($titles, SORT_ASC);
            foreach ($titles as $title) {
                $id = explode('.', $title);
                $id = $id[0];
                echo "<img class='title' id='" . $id . "' style='display:block; margin-top:5px; width:320px' src='" . base_url($this->uri->segment(1) . '/assets/' . $templateAlias . '/images/titles/' . $title) . "'>";
            }
        }
    }

    function landingpage($pk_templates_saved = '', $ajax = '') {
        //echo $pk_templates_saved;
        $this->load->model('templates_model');
        $result = $this->templates_model->loadSaveMail($pk_templates_saved);

        echo '<html><head>';
        echo '<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
              <meta charset="utf-8">
              <meta name="viewport" content="width=device-width, initial-scale=1.0">
              <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>';

        echo '<script>'
        . '$(document).ready(function(){'
        . '$("table").css({margin:"0 auto"});'
        . '});'
        . '</script>';

        echo '</head><body>';
        echo $result[0]->landingCode;

        echo '</body></html>';
    }

    function loadHTML($pk_templates_saved = '') {
        //echo $pk_templates_saved;
        $this->load->model('templates_model');
        $result = $this->templates_model->loadSaveMail($pk_templates_saved);

        header("Content-Type: plain/text");

        echo $result[0]->landingCode;
    }

    function loadRawCode($pk_templates_saved = '') {
        //echo $pk_templates_saved;
        $this->load->model('templates_model');
        $result = $this->templates_model->rawCode($pk_templates_saved);
        return $result[0];
    }

    function delete($pk_templates_saved = "") {
        $this->load->model('templates_model');
        $result = $this->templates_model->deleteRow($pk_templates_saved);

        if ($result) {
            echo "OK";
        } else {
            echo "Application Error";
        }
    }

}
