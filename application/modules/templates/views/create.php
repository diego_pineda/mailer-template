<?php 
?>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <?php 
               if(isset($SelectedLayout)){
                   
                    $id_layout = $SelectedLayout[0]->pk_templates_layout;
                    echo 'New '.$SelectedLayout[0]->name.' Template Creator';
                }else{ 
                    echo "edit Template"; 
                    
                    $id_layout = $SelectedTemplate->fk_templates_layout;
                    $id_template = $SelectedTemplate->pk_templates_saved;
                } 
                ?>
            </header>
            <div class="panel-body">
                <br>
                <form class="form-horizontal" role="form">


                    <div class="form-group">
                        <label for="template_editable" class="col-lg-2 col-sm-2 control-label"></label>
                        <div class="col-lg-10">
                            <div id="" ></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="template_editable" class="col-lg-2 col-sm-2 control-label">Preview</label>
                        <div class="col-lg-10">
                            <div id="template_editable" style="background-color: #eee;width: 100%; overflow: hidden; border:solid #ccc 1px" >

                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="template_editable" class="col-lg-2 col-sm-2 control-label">Operations</label>
                        <div class="col-lg-offset-2 col-lg-10" id="loadModules">

                        </div>
                    </div>
                </form>

            </div>
        </section>

    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Load A Module</h4>
            </div>
            <div class="modal-body">

                Please Select a Module...
                <div class="form-group">

                    <div class="col-lg-10" id="modules_content">


                    </div>
                </div>
                <?php if ($templateAlias == 'internal') { ?>
                    <div class="form-group">
                        <label for="fk_templates_layout" class="col-lg-3 col-sm-2 control-label">Position:</label>
                        <select id="position" name="position" class="form-control">
                            <option value="modIzquierda" >Left (Izquierda)- Default</option>
                            <option value="modDerecha" >Right (Derecha)</option>
                        </select>
                    </div>
                <?php } else { ?>
                    <input name="position" type="hidden" id="position" value="modDefault">
                <?php } ?>

            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">close</button>
                <button data-dismiss="modal" class="btn btn-success" type="button" onclick="loadSelectedModule()" >load module</button>
            </div>
        </div>
    </div>
</div>
<!-- modal replace Images -->
<div class="modal fade" id="selectImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="contentLoadImg" >


        </div>
    </div>
</div>
<!-- model edit Links --> 
<div class="modal fade" id="insertLink" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" id="contentloadLink">


        <!-- load Link Data for change --> 

    </div>
</div>
<!-- modal save template-->
<div class="modal fade" id="savePrompt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Save template</h4>
            </div>
            <div class="modal-body">

                Please Enter a template name
                <div class="col-lg-10" id="prompt_content">

                </div>


            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">cancel</button>
                <button data-dismiss="modal" class="btn btn-success" id="btnSaveTemplate" type="button"  >save</button>
            </div>
        </div>
    </div>
</div>

<!-- save prompt 2 -->

<div class="modal fade" id="savePrompt2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Save template</h4>
            </div>
            <div class="modal-body">

                Please Enter a template name
                <div class="col-lg-10" id="prompt_content2">

                </div>


            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">cancel</button>
                <button data-dismiss="modal" class="btn btn-success" id="btnSaveTemplate2" type="button"  >save</button>
            </div>
        </div>
    </div>
</div>

<!--modal select titles-->
<div class="modal fade" id="selectTitle" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" id="contentTitles" >
    </div>
</div>
<!--modal view Source-->
<div class="modal fade" id="viewSource" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" id="contentSource">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">View  Source code:</h4>
            </div>
            <div class="modal-body" style="height: 4s5px;" >

                <div class="form-group">
                    <label for="Link" class="col-lg-3 col-sm-2 control-label">Source Code:</label>
                    <textarea readonly="readonly" style="height: 376px; cursor: pointer" name="source" class="form-control" id="source"></textarea>
                </div>
                <div> To copy the sourceCode: click the text area, then ctrl+C </div>

            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-success" type="button">close</button>

            </div>
        </div>

    </div>
</div>

<!-- modal -->
<input type="hidden" value="<?= $id_layout ?>" id="layout">
<script>
    function loadLayout(layoutid,templateId) {

        
        if(typeof templateId=='undefined'){
          var loadUrl = "<?= base_url('templates/loadLayoutAjax') ?>/" + layoutid;
      }else{
          var loadUrl = "<?= base_url('templates/loadTemplateAjax') ?>/" + templateId;
          }
        $.get(loadUrl, function(data) {
            $("#template_editable").html(data);

        }).done(function() {


            var marginLeftCSS = ($("#template_editable").width() - $("#mail_" + layoutid).width()) / 2;

            $("#mail_" + layoutid).css({marginLeft: marginLeftCSS});
            $("#mail_2").css({backgroundColor: '#d0d0ce'});
            //$("#template_editable").height($("#mail_" + layoutid).height() + 10);
            $('#template_editable font').addClass('editable-field').delay(3000);
            $.ajax({
                url: "<?php echo base_url('assets/administrator'); ?>/js/mailer_js.js",
                dataType: "script"
            });



        });

        $("#loadModules").html('<a class="btn btn-success" id="module_load" data-toggle="modal" href="#myModal" onclick="loadModule(' + layoutid + ')">load a module</a>');
        <?php if ($this->uri->segment(3)=='edit'){ ?>
        $("#loadModules").append('<a style="float:right;" id="btnSavePrompt2" class="btn btn-info" data-toggle="modal" href="#savePrompt2" >save template</a>');
        <?php }else{ ?>
         $("#loadModules").append('<a style="float:right;" id="btnSavePrompt" class="btn btn-info" data-toggle="modal" href="#savePrompt" >save template</a>');  
        <?php } ?>
        $("#loadModules").append('<a style="display:none;" id="btnViewSource" class="btn btn-info" data-toggle="modal" href="#viewSource" >view source</a>');





    }
    function loadModule(layoutid) {
        $.get("<?= base_url('templates/loadModulesAjax') ?>/" + layoutid, function(data) {
            $("#modules_content").html(data);

        });
    }
    function loadSelectedModule() {
        var module = $('#modules_loaded').val();
        var position = $('#position').val();

        $.get("<?= base_url('templates/loadModulesbyID') ?>/" + module, function(data) {


            if ($("#modDefault").length) {
                $("#modDefault").append(data);
            } else {
                $("#" + position).append(data);
                //console.log("#" + position);
            }

            $.ajax({
                url: "<?php echo base_url('assets/administrator'); ?>/js/mailer_js.js",
                dataType: "script"
            });

            // $("#template_editable").height($("#mail_<?= $id_layout; ?>").height());

        }).done(function() {
            $('#template_editable font').addClass('editable-field');
        });
    }

    function loadImage(elementID) {
        var url = "<?= base_url('templates/saveImageData') ?>";
        var data = {};
        var oldWidth = $("#" + elementID).attr('width');
        if (typeof $('.fileupload-preview>img').attr('src') === 'undefined') {
            return false;
        } else {
            if ($('.fileupload-preview>img').attr('src') == 'nothing') {
                $("#" + elementID).remove();
                
            } else {

                data['image_as_base64'] = $('.fileupload-preview>img').attr('src');
                $("#" + elementID).attr('width', '100');
                if(elementID!='revista'){
                    $("#" + elementID).parent().css({backgroundColor: '#FFFFFF'})
                }else{
                    $("#" + elementID).parent().css({backgroundColor: 'none'})
                }
                $("#" + elementID).attr('src', '<?= base_url('assets/administrator/img/ajax-loader.gif') ?>');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data
                }).done(function(response) {
                    if (response != 'error') {
                        //console.log(response);
                        $("#" + elementID).attr('src', response);
                        $("#" + elementID).attr('width', oldWidth);
                    } else {
                        alert('error: something happend, please try Again');
                    }

                });
            }
        }


    }
<?php
if ($id_layout) {
    if($this->uri->segment(3)=='edit'){
    echo "loadLayout(" . $id_layout . "," . $id_template . ")";
    }else{
    echo "loadLayout(" . $id_layout . ")";  
    }
}
?>
<?php if($this->uri->segment(3)=='edit'){ ?>
    
$("#btnSavePrompt2").click(function() {

    var template_name = '<?=$SelectedTemplate->name?>';
    var htmlstring = '<div class="form-group">'
            + '<label for="template_name" class="col-lg-3 col-sm-2 control-label"></label>'
            + '<input type="text" name="template_name" class="form-control" id="template_name" placeholder="" value="' + template_name + '">'
            + '</div>'
    $("#prompt_content2").html(htmlstring);
});

$("#btnSaveTemplate2").click(function() {
    
    if (_controlSave == 0) {


        var data = {};
       
        data['rawCode'] =$("#template_editable").html();
        var url = document.URL.split('/create');
        
        url = url[0] + '/savetemplate2/<?=$id_template;?>';
        console.log(url);
        var bg = $("#mail_" + $("#layout").val()).css('background-color');
        $("#mail_" + $("#layout").val()).attr('style', 'background-color:' + bg + '; margin:0 auto;');
        $('.delete-module').remove();

        $.each($("#template_editable font"), function(index, item) {


            var mainStyle = $(item).attr('style');
            

            $.each($($(item)).children("div"), function(indice, objeto) {
                $(objeto).attr('style', mainStyle);
                $(objeto).css({margin:"0 0 0 0"});
            });


        });



        var sourceCode = $("#template_editable").html();
        data['sourceCode'] = sourceCode;
        data['name'] = $("#template_name").val();
        data['fk_templates_layout'] = $("#layout").val();
        //console.log(data);
        //return false;
        $.ajax({
            type: "POST",
            url: url,
            data: data
        }).done(function(response) {

            $("#template_editable").html(response);

            $("#btnSavePrompt2").hide();
            $("#module_load").hide();
            $("#btnViewSource").show();


        });
        _controlSave++;
    }

});
<?php } ?>
</script>