<!--Encuentros Natura-->
<div style="margin:0;position: relative; " id="<?= date('U') ?>" class="default_mod">
    <table width="368" border="0" cellspacing="0" cellpadding="0"  >
        <tr>
            <td bgcolor="#e2d9e1">
                <table width="800" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="bottom"><table width="346" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td valign="bottom"><img style="display:block" src="<?= base_url("templates/assets") ?>/external/images/encuentros_natura.jpg" width="346" height="100" /><font face="Arial, Helvetica, sans-serif" style="font-family:Arial, Helvetica, sans-serif; margin:0 0 5px 50px; display:block; font-size:24px; color:#5e5e5e; width:200px;" >Vivencia Chronos</font>
                                        <font style="font-family:Arial, Helvetica, sans-serif; margin:0 0 0 50px; display:block;font-size:14px; color:#5e5e5e; width:190px;line-height: 14px;" class="editable-field ">Hay uno para tu historia</font>
                                    </td>
                                </tr>
                                <tr>
                                    <td  height="140" valign="bottom" bgcolor="#e2d9e1">
                                        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                                            <tr><td width="50" >&nbsp;</td><td><font  style="line-height: 15px;font-family:Arial, Helvetica, sans-serif;  display:block; font-size:14px; color:#5e5e5e; width:210px;">¡No te pierdas este encuentro! Tu rostro cuenta una historia.<br>Conoce el lugar, fecha y hora de tu encuentro Natura.</font></td></tr>
                                        </table>
                                        <a contenteditable="false" class="editable-field maxchar[200]" href="" title="Haz click aquí" style="font-family:Arial, Helvetica, sans-serif; margin:20px 30px 0 0; display:block; font-size:14px; float:right; color:#5e5e5e; width:100px; line-height: 14px;" >Haz click aquí &raquo;</a>
                                    </td>
                                </tr>
                                
                            </table></td>
                        <td><table width="454" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td rowspan="3" valign="bottom"><img style="display:block"src="<?= base_url("templates/assets") ?>/external/images/encuentro_foto1.jpg" width="33" height="305" /></td>
                                    <td><img style="display:block"src="<?= base_url("templates/assets") ?>/external/images/encuentro_foto2.jpg" width="374" height="44" /></td>
                                    <td rowspan="3" valign="bottom" ><img style="display:block"src="<?= base_url("templates/assets") ?>/external/images/encuentro_foto3.jpg" width="47" height="305" /></td>
                                </tr>
                                <tr>
                                    <td valign="bottom" ><img style="display:block" id="encuentros" class="edit_image" data-toggle="modal"  href="#selectImage" src="http://www.placehold.it/374x251/cccccc/AAAAAA&amp;text=load+image+size:374x251" width="374" height="251" /></td>
                                </tr>
                                <tr>
                                    <td valign="bottom" ><img style="display:block"  src="<?= base_url("templates/assets") ?>/external/images/encuentro_foto4.gif" width="374" height="10" /></td>
                                </tr>
                                
                                
                            </table></td>
                    <tr>
                        <td valign="bottom"><img style="display:block"src="<?= base_url("templates/assets") ?>/external/images/encuentro_end.jpg" width="346" height="69" /></td><td valign="bottom"><img style="display:block"src="<?= base_url("templates/assets") ?>/external/images/encuentro_foto5.jpg" width="454" height="69" /></td>
                    </tr>
                    </tr>
                </table>
            </td>
        </tr>
        <!--Fin Encuentros Natura -->
        <!--revista natura-->
        <tr>
            <td bgcolor="#594c7a">
                <table width="800" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="bottom"><table width="454" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><img style="display:block"src="<?= base_url("templates/assets") ?>/external/images/revista_tit.gif" width="490" height="76" /></td>
                                </tr>
                                <tr>
                                    <td >
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="50">&nbsp;</td>
                                                <td style="padding-right:50px;"><font style="line-height: 14px;display:block; margin:10px 0 28px 0; font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#FFF">Conoce todas las novedades que trae la revista de ciclo 3. </font>
                                                    <font style="line-height: 14px;display:block; margin-bottom:28px; font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#FFF">Con el lanzamiento del nuevo perfume Humor Bom Bom tú y tus clientes continuarán enamorándose de la marca Natura. </font>
                                                    <font style="display:block; margin-bottom:28px; font-size:14px; font-family:Arial, Helvetica, sans-serif; color:#FFF">Además de todos los descuentos y lanzamientos de las otras líneas que tenemos para ti.</font></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="110"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="50">&nbsp;</td>
                                                <td width="230"><font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#FFFFFF;">Escoge cómo ver la revista en:</font> </td> 
                                                <td>
                                                    <a href="#" title="pdf" style="width:76px; display:inline-block;"><img style="display:block"src="<?= base_url("templates/assets") ?>/external/images/revista_pdf.gif" width="70" height="106" /></a>
                                                </td>
                                                <td>
                                                    <a href="#" title="Enlinea" style="width:76px; margin:0 0 0 20px"><img style="display:block"src="<?= base_url("templates/assets") ?>/external/images/revista_enlinea.gif" width="70" height="106" /></a>
                                                </td>
                                            </tr>
                                        </table></td>
                                </tr>

                            </table></td>
                        <td valing="bottom"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td  height="30" valign="bottom"><img style="display:block"id="revista" class="edit_image" data-toggle="modal"  href="#selectImage" src="http://www.placehold.it/310x360/594C7A/AAAAAA&amp;text=load+PNG+Image+alpha+Transparency" alt="" width="310" height="360" /></td>
                                </tr>

                            </table></td>
                    </tr>

                    <tr>
                        <td height="30" valign="bottom"><img style="display:block"src="<?= base_url("templates/assets") ?>/external/images/revista_end.jpg" width="490" height="18" /></td>
                        <td height="30" valign="bottom"><img style="display:block"src="<?= base_url("templates/assets") ?>/external/images/revista_end.jpg" alt="" width="310" height="18" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div data-element="<?= date('U') ?>" class="delete-module" style="left: 775px;">x</div> 
    <!--Fin Revista Natura-->