<?php $section = $aliasTemplate ?>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Templates
            </header>
            <div class="panel-body">

                <div class="row state-overview">
                    <div class="col-lg-3 col-sm-6">

                        <a href="<?= base_url('templates/view/mobile') ?>">
                            <section class="panel" <?= ($section == 'mobile') ? 'style="background-color:#eee;"' : ' '; ?>>
                                <div class="symbol terques">
                                    <i class="fa fa-phone-square"></i>
                                </div>
                                <div class="value">
                                    <h1 class="">Mobile</h1>
                                    <p>&nbsp;</p>
                                </div>
                            </section>
                        </a>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <a href="<?= base_url('templates/view/internal') ?>">
                            <section class="panel" <?= ($section == 'internal') ? 'style="background-color:#eee;"' : ' '; ?>>
                                <div class="symbol red">
                                    <i class="fa fa-book"></i>
                                </div>
                                <div class="value">
                                    <h1 class="">Internal Mail</h1>
                                    <p>&nbsp;</p>
                                </div>
                            </section>
                        </a>
                    </div> 
                    <div class="col-lg-3 col-sm-6">
                        <a href="<?= base_url('templates/view/external') ?>">
                            <section class="panel" <?= ($section == 'external') ? 'style="background-color:#eee;"' : ' '; ?>>
                                <div class="symbol yellow">
                                    <i class="fa fa-cloud"></i>
                                </div>
                                <div class="value">
                                    <h1 class="">External Mail</h1>
                                    <p>&nbsp;</p>
                                </div>
                            </section>
                        </a>
                    </div>


                </div>

            </div>
        </section>
        <?php if ($section != '') { ?>
            <section class="panel">
                <header class="panel-heading">
                    <?= strtoupper($section) ?>
                </header>
                <div class="panel-body">
                    <div class="adv-table">
                        <a href="<?= base_url('templates/create/' . $section); ?>" class="btn btn-primary"><i class="fa fa-th"></i> New template</a>


                        <table  class="display table table-bordered table-striped" id="data_table">
                            <thead>
                                <tr>
                                    <th class="center hidden-phone" style="width: 30px">Id.</th>
                                    <th>Name</th>
                                    <th>Date</th>

                                    <th class="center" style="width: 30px">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($templates as $template): ?>
                                    <tr class="" id="row_<?= $template->pk_templates_saved ?>">
                                        <td class="center hidden-phone"><?= $template->pk_templates_saved; ?></td>
                                        <td><?= $template->name; ?></td>
                                        <td><?= $template->date_added; ?></td>

                                        <td >
                                            <div class="btn-group">
                                                <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><i class="fa "></i> Actions <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a data-id="<?= $template->pk_templates_saved; ?>" class="btnPreview" data-toggle="modal" href="#previewModal" >Preview</a></li>
                                                    <li><a data-id="<?= $template->pk_templates_saved; ?>" class="btnViewSource"  data-toggle="modal" href="#viewSource" >View Source</a></li>
                                                    <li><a data-id="<?= $template->pk_templates_saved; ?>" class="btnEdit"  data-toggle="modal" href="<?php echo base_url('templates/create/edit/'.$template->pk_templates_saved) ?>" >Edit</a></li>

                                                    <li class="divider"></li>
                                                    <li><a href="#" class="btnDelele" data-id="<?= $template->pk_templates_saved ?>">Delete</a></li>
                                                </ul>

                                            </div>
                                        </td>
                                    </tr>

                                <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="center hidden-phone">Id.</th>
                                    <th>Name</th>
                                    <th>Date</th>


                                    <th class="center">Actions</th>
                                </tr>
                            </tfoot>
                        </table>
                        
                    </div>
                </div>
            </section>
        <?php } ?>
    </div>
</div>
<!-- modalBox Begin-->
                        <div class="modal fade" id="previewModal" tabindex="-1" role="dialog" aria-labelledby="previewmodal" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content" style="width: 850px; height: 580px">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">preview Template</h4>
                                    </div>
                                    <div class="modal-body" style="width: 850px; height: 440px">


                                        <div class="form-group">

                                            <div class="col-lg-10" id="previewContent">


                                            </div>
                                        </div>


                                    </div>
                                    <div class="modal-footer">
                                        <button data-dismiss="modal" class="btn btn-default" type="button">close</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modalBox End -->
                        <!--modal view Source-->
                        <div class="modal fade" id="viewSource" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog" id="contentSource">

                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">View  Source code:</h4>
                                    </div>
                                    <div class="modal-body" style="height: 4s5px;" >

                                        <div class="form-group">
                                            <label for="Link" class="col-lg-3 col-sm-2 control-label">Source Code:</label>
                                            <textarea readonly="readonly" style="height: 376px; cursor: pointer" name="source" class="form-control" id="source"></textarea>
                                        </div>
                                        <div> To copy the sourceCode: click the text area, then ctrl+C </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button data-dismiss="modal" class="btn btn-success" type="button">close</button>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--modal view Source-->

<script>
    $(".btnPreview").click(function() {
        var elementID = $(this).attr('data-id');
        var url = '<?= base_url('templates/landingpage') ?>/' + elementID ;
        var prevIframe = '<iframe src="' + url + '"  width="840" height="430" style="margin-left: -30px;"></iframe>';
        $('#previewContent').html(prevIframe);


    });
    $(".btnViewSource").click(function() {
        console.log('hola');
        var templateid = $(this).attr('data-id');
        var url = '<?= base_url('templates/loadHTML') ?>/' + templateid;
        console.log(url);

        $.ajax({
            type: "POST",
            url: url,
            data: '',
            dataType: 'text'
        }).done(function(response) {
            $('#source').html(response);

            $('#source').click(function() {
                $('#source').select();
                $('#source').focus();
            });

        });


    });



    $(".btnDelele").click(function(e) {
        e.preventDefault();
        var elementID = $(this).attr('data-id');
        var url = '<?= base_url('templates/delete') ?>/' + elementID;
        $.ajax({
            type: "POST",
            url: url,
            data: ''
        }).done(function(r) {
            console.log(r);
            console.log('#row_' + elementID);
            $('#source').html(r);
            if (r = 'OK') {
                $('#row_' + elementID).remove();
            } else {
                alert("Cant delete a this template: " + r);
            }

        });

    });




</script>