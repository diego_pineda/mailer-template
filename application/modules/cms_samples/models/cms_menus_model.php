<?php

/**
 * Administrator_model 
 * 
 * @category Category 
 * @author Diego F. Pineda
 * @email diegopineda@latinmedios.com
 * @agency Latinmedios INC. 
 */
class Cms_menus_model extends CI_Model {

    var $upload_path;

    function __construct() {
        parent::__construct();
        $this->upload_path = realpath(APPPATH . "../application/uploads");
        $this->load->model('../cms/models/administrator_model');
    }

    /**
     * Validate function
     * @param array $param1 data username and Password of person
     * @return Query Object 
     */

    function menuList() {
        
       
       
        $query = $this->db->get("tbl_cms_menu");
        return $query->result();
    }
    
    function getUser($pk_user='') {
        
        if($pk_user!=''){
            $this->db->select('tbl_cms_users.*');
            $this->db->select('tbl_cms_roles.name as role');
            $this->db->from('tbl_cms_users');
            $this->db->join('tbl_cms_roles', 'tbl_cms_roles.pk_role = tbl_cms_users.fk_role');
            $this->db->where('tbl_cms_users.fk_status', 1);
            $this->db->where('tbl_cms_users.pk_user', $pk_user);
            $query = $this->db->get();
            return $query->result();
        
        }else{
            return false;
        }
        
    }
    
    
    
//
//    function cmsConfig() {
//        $this->db->order_by('pk_config', 'asc');
//        $query = $this->db->get('tbl_cms_config');
//        return $query->result();
//    }

}
