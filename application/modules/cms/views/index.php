<!DOCTYPE html>
<html lang="en">
  
<!-- Mirrored from thevectorlab.net/flatlab/ by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 19 Mar 2014 19:45:54 GMT -->
<?php echo $head; ?>
  <body>

  <section id="container" >
      <!--header start-->
<?php echo $header ?>      
      <!--header end-->
      <!--sidebar start-->
      
<?php echo $aside; ?>      
      
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
              <!--state overview start-->
              
           <?php echo $mainview; ?>     

          </section>
      </section>
      <!--main content end-->
      <!--footer start-->
      <?php echo $footer; ?>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->

    <script src="<?php echo base_url($cms_config[0]->admin_assets); ?>/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url($cms_config[0]->admin_assets); ?>/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo base_url($cms_config[0]->admin_assets); ?>/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url($cms_config[0]->admin_assets); ?>/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?php echo base_url($cms_config[0]->admin_assets); ?>/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="<?php echo base_url($cms_config[0]->admin_assets); ?>/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="<?php echo base_url($cms_config[0]->admin_assets); ?>/js/owl.carousel.js" ></script>
    <script src="<?php echo base_url($cms_config[0]->admin_assets); ?>/js/jquery.customSelect.min.js" ></script>
    <script src="<?php echo base_url($cms_config[0]->admin_assets); ?>/js/respond.min.js" ></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url($cms_config[0]->admin_assets); ?>/assets/advanced-datatable/media/js/jquery.js"></script>
   
    <script class="include" type="text/javascript" src="<?php echo base_url($cms_config[0]->admin_assets); ?>/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo base_url($cms_config[0]->admin_assets); ?>/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url($cms_config[0]->admin_assets); ?>/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript" src="<?php echo base_url($cms_config[0]->admin_assets); ?>/assets/advanced-datatable/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?php echo base_url($cms_config[0]->admin_assets); ?>/assets/data-tables/DT_bootstrap.js"></script>
    <script src="<?php echo base_url($cms_config[0]->admin_assets); ?>/js/respond.min.js" ></script>
   
    <script src="<?php echo base_url($cms_config[0]->admin_assets); ?>/js/common-scripts.js"></script>

     <!-- bootstrap-wysihtml5 -->   
    <link rel="stylesheet" href="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <script src="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
    <script src="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
    
     <!-- Bootstrap image -->
    <link href="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet">
    <script src="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/bootstrap-fileupload/bootstrap-fileupload.js"></script> -->

    
    <!--  bootstrap-colorpicker -->
    <link rel="stylesheet" href="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/bootstrap-colorpicker/css/colorpicker.css" />
    <script src="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
    

    <!--script for Graphics and Charts -->
<!--    <script src="<?php echo base_url($cms_config[0]->admin_assets); ?>/js/sparkline-chart.js"></script>
    <script src="<?php echo base_url($cms_config[0]->admin_assets); ?>/js/easy-pie-chart.js"></script>-->
    <script src="<?php echo base_url($cms_config[0]->admin_assets); ?>/js/count.js"></script>

    <!--script for Crop Images -->
    <script src="<?php echo base_url($cms_config[0]->admin_assets); ?>/assets/jcrop/js/jquery.color.js"></script>
    <script src="<?php echo base_url($cms_config[0]->admin_assets); ?>/assets/jcrop/js/jquery.Jcrop.min.js"></script>


     <!-- validation engine -->
    <link rel="stylesheet" href="<?= base_url($cms_config[0]->admin_assets.'/validationengine/css/validationEngine.jquery.css') ?>" type="text/css"/>
    <script src="<?= base_url($cms_config[0]->admin_assets.'/validationengine/js/languages/jquery.validationEngine-en.js') ?>" type="text/javascript" charset="utf-8">
    </script>
    <script src="<?= base_url($cms_config[0]->admin_assets.'/validationengine/js/jquery.validationEngine.js') ?>" type="text/javascript" charset="utf-8"></script>


    <!-- custom functions -->
    <script src="<?php echo base_url($cms_config[0]->admin_assets); ?>/js/script.js"></script>

    <script type="text/javascript" src="<?php echo base_url($cms_data[0]->admin_assets); ?>/js/jquery.zclip.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url($cms_data[0]->admin_assets); ?>/js/jquery.snippet.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url($cms_data[0]->admin_assets); ?>/js/zeroclipboard/ZeroClipboard.js"></script>
    
  <script>
ZeroClipboard.config( { swfPath: '<?php echo base_url($cms_data[0]->admin_assets); ?>/js/zeroclipboard/ZeroClipboard.swf' } );

var client = new ZeroClipboard($("#copyCode"));
$(document).ready(function(){

});

      //owl carousel

//      $(document).ready(function() {
//          $("#owl-demo").owlCarousel({
//              navigation : true,
//              slideSpeed : 300,
//              paginationSpeed : 400,
//              singleItem : true,
//        autoPlay:true
//
//          });
//      });

      //custom select box

//      $(function(){
//          $('select.styled').customSelect();
//      });


     //dinamic table
      
          $(document).ready(function() {
              $('#data_table').dataTable( {
                 
              } );
          } );
      
  </script>

  </body>

<!-- Mirrored from thevectorlab.net/flatlab/ by HTTrack Website Copier/3.x [XR&CO'2013], Wed, 19 Mar 2014 19:46:30 GMT -->
</html>
