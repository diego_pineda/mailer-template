<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo $cms_data[0]->description ?>">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="<?php echo $cms_data[0]->description ?>">
    <link rel="shortcut icon" href="img/favicon.html">

    <title><?php echo $cms_data[0]->sitename ?></title>


    
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url($cms_data[0]->admin_assets); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url($cms_data[0]->admin_assets); ?>/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<!--    <link href="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <link rel="stylesheet" href="<?php echo base_url($cms_data[0]->admin_assets); ?>/css/owl.carousel.css" type="text/css">-->
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url($cms_data[0]->admin_assets); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url($cms_data[0]->admin_assets); ?>/css/style-responsive.css" rel="stylesheet" />
    
   <!-- Custom styles for data Tables  -->
   <link rel="stylesheet" href="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/data-tables/DT_bootstrap.css" />
    <!-- Jquery Library load to be available  -->
    <!--<script src="<?php echo base_url($cms_data[0]->admin_assets); ?>/js/jquery.js"></script>-->
    <script src="<?php echo base_url($cms_data[0]->admin_assets); ?>/js/jquery-1.8.3.min.js"></script>

  
   
  <!--    bootstrap-timepicker
      <link rel="stylesheet" href="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/bootstrap-timepicker/compiled/timepicker.css" />
      <script src="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
  
      pinner   
      <link rel="stylesheet" href="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/fuelux/css/tree-style.css" />
      <script src="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/fuelux/js/spinner.min.js"></script>
      <script src="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/fuelux/js/tree.min.js"></script> 
      
      bootstrap-daterangepicker
      <link rel="stylesheet" href="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/bootstrap-daterangepicker/daterangepicker.css" />
      <script src="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/bootstrap-daterangepicker/daterangepicker.js"></script> 
  
      bbootstrap-datepicker
      <link rel="stylesheet" href="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/bootstrap-datepicker/css/datepicker.css" />
      <script src="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
  
      bootstrap-datetimepicker
      <link rel="stylesheet" href="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/bootstrap-datetimepicker/css/datetimepicker.css" />
      <script src="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
      
 
      
  jquery-multi-select
      <link rel="stylesheet" href="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/jquery-multi-select/css/multi-select.css" />
      <script src="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/jquery-multi-select/js/jquery.multi-select.js"></script>
      <script src="<?php echo base_url($cms_data[0]->admin_assets); ?>/assets/jquery-multi-select/js/jquery.quicksearch.js"></script> -->

     

    <!-- form -->
    <!--<script src="<?php echo base_url($cms_data[0]->admin_assets); ?>/js/advanced-form-components.js"></script>  -->
    
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="<?php echo base_url($cms_data[0]->admin_assets); ?>/js/html5shiv.js"></script>
      <script src="<?php echo base_url($cms_data[0]->admin_assets); ?>/js/respond.min.js"></script>
    <![endif]-->
    

  </head>
