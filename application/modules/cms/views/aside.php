 <?php
$section =  $this->uri->segment(1);
$client = $this->session->userdata['client'];
$logo=$this->session->userdata['logo'];
?>
<aside>
    
    <div id="sidebar"  class="nav-collapse ">
        
            <?php if ($logo){ ?>
        <div class="company-logo" style="margin-top: 60px; margin-bottom: -60px">
            <img  style="margin: 0 auto; display:block; width: 140px" src="<?php echo base_url('assets/administrator/images').'/'.$this->session->userdata['logo'];?>" >
          </div>  
            <?php }  ?>
        
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
                      <?php   if ($client==1){ ?>
                            <li class="sub-menu" >
                                <a  href="javascript:;" <?php echo ($section =='cms_config'||$section =='cms_users'||$section =='cms_menu' ) ? "class='active'":" "; ?>>
                                    <i class="fa fa-gears"></i>
                                    <span>CMS Administration</span>
                                </a>
                                <ul class="sub">
                                     
                                    <!--<li  >
                                        <a  href="<?php echo base_url("cms_config") ?>">
                                            <i class="fa fa-gear"></i>
                                            <span>Configuration</span>
                                        </a>
                                    </li>-->
                                   
                                    <li <?php echo ($section =='cms_users') ? "class='active'":" "; ?> >
                                        <a  href="<?php echo base_url("cms_users") ?>">
                                            <i class="fa fa-user"></i>
                                            <span>Users</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a  href="<?php echo base_url("cms_menus") ?>">
                                            <i class="fa fa-th-list"></i>
                                            <span>Menu (modules)</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a  href="<?php echo base_url("cms_samples") ?>">
                                            <i class="fa fa-th-list"></i>
                                            <span>Samples smipets</span>
                                        </a>
                                        <ul class="sub">
                                     
                                    <li  >
                                        <a  href="<?php echo base_url("cms_samples/from_components") ?>">
                                            
                                            <span>Form Components</span>
                                        </a>
                                    </li>
                                    <li  >
                                        <a  href="<?php echo base_url("cms_samples/upload_components") ?>">
                                            
                                            <span>Upload Components</span>
                                        </a>
                                    </li>

                                    <li  >
                                        <a  href="http://thevectorlab.net/flatlab/index.html" target="_blank">
                                            <span>Sample site</span>
                                        </a>
                                    </li>
                                   
                                    
                                </ul>
                                    </li>
                                </ul>
                            </li>
                      <?php } ?>
            <li>
                <a class="active" href="<?php echo base_url('cms') ?>/">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>


            <!--multi level menu start-->
<!--            <li class="sub-menu">
                <a href="javascript:;" >
                    <i class="fa fa-sitemap"></i>
                    <span>Multi level Menu</span>
                </a>
                <ul class="sub">
                    <li><a  href="javascript:;">Menu Item 1</a></li>
                    <li class="sub-menu">
                        <a  href="boxed_page.html">Menu Item 2</a>
                        <ul class="sub">
                            <li><a  href="javascript:;">Menu Item 2.1</a></li>
                            <li class="sub-menu">
                                <a  href="javascript:;">Menu Item 3</a>
                                <ul class="sub">
                                    <li><a  href="javascript:;">Menu Item 3.1</a></li>
                                    <li><a  href="javascript:;">Menu Item 3.2</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>-->
            <!--multi level menu end-->

        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
<script>
    $(function() {
        var granfather = 0;
        $.each(<?php echo($cms_menu) ?>, function(k, v) {
            
            if (v.url != '') {
                var url = '<?php echo base_url() ?>' + v.url;
                var section = v.url.split('/');
                section = section[0];
            } else {
                var url = 'javascript:;';
                var section = '';
            }
            var urlActual = document.URL.split('/');
//            console.log('actualURL= '+urlActual[3]);
//            console.log('Section= '+section);
            if(urlActual[3]==section){
                var myclass = 'class="active"';
                }else{
                 var myclass = ' ';   
                    }
            
            if (granfather == v.fk_menu) {
                        
                var parents = '<li  class="sub-menu ">'
                        + '<a '+myclass+'  title="' + v.alt_text + '" alt="' + v.alt_text + '" href="' + url + '" >'
                        + '<i class="fa fa-' + v.icon + '"></i>'
                        + '<span>' + v.name + '</span>'
                        + '</a><ul id="father_' + v.pk_menu + '" class="sub"></ul></li>';
                $('.sidebar-menu').append(parents);
            } else {

                var childs = '<li  class="sub-menu">'
                        + '<a title="' + v.alt_text + '" alt="' + v.alt_text + '" href="' + url + '">' + v.name + '</a>'
                        + '<ul id="father_' + v.pk_menu + '" class="sub child">'

                        + '</ul>'
                        + '</li>';
                $('#father_' + v.fk_menu).append(childs);
            }


        });
        // fix to Autoremove the UL tag when is empty
        $('ul.sub').each(function(k, e) {

            if ($(e).html() == '') {
                $(e).remove();
            }
        });

    });
    
</script>