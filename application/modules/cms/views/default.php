<section class="panel">

    <div class="panel-body">
        <div class="value">
            <h1 class="">dashboard</h1>
            <p>&nbsp;</p>
        </div>
        

    </div>
</section>
<section class="panel">
    <header class="panel-heading">
        Templates
    </header>
    <div class="panel-body">

        <div class="row state-overview">
            <div class="col-lg-3 col-sm-6">

                <a href="<?= base_url('templates/view/mobile') ?>">
                    <section class="panel" >
                        <div class="symbol terques">
                            <i class="fa fa-phone-square"></i>
                        </div>
                        <div class="value">
                            <h1 class="">Mobile</h1>
                            <p>&nbsp;</p>
                        </div>
                    </section>
                </a>
            </div>
            <div class="col-lg-3 col-sm-6">
                <a href="<?= base_url('templates/view/internal') ?>">
                    <section class="panel" >
                        <div class="symbol red">
                            <i class="fa fa-book"></i>
                        </div>
                        <div class="value">
                            <h1 class="">Internal Mail</h1>
                            <p>&nbsp;</p>
                        </div>
                    </section>
                </a>
            </div> 
            <div class="col-lg-3 col-sm-6">
                <a href="<?= base_url('templates/view/external') ?>">
                    <section class="panel" >
                        <div class="symbol yellow">
                            <i class="fa fa-cloud"></i>
                        </div>
                        <div class="value">
                            <h1 class="">External Mail</h1>
                            <p>&nbsp;</p>
                        </div>
                    </section>
                </a>
            </div>


        </div>

    </div>
</section>
<section class="panel">
    <header class="panel-heading">
        Blog
    </header>
    <div class="panel-body">

        <div class="row state-overview">
            <div class="col-lg-3 col-sm-6">

                <a href="<?= base_url('cms_blog/category_list') ?>">


                    <section class="panel" >
                        <div class="symbol blue">
                            <i class="fa fa-th-large"></i>
                        </div>
                        <div class="value">
                            <h1 class="">Categories</h1>
                            <p>&nbsp;</p>
                        </div>
                    </section>
                </a>
            </div>
            <div class="col-lg-3 col-sm-6">
                <a href="<?= base_url('cms_blog/article_list') ?>">

                    <section class="panel" >
                        <div class="symbol panel green-chart ">
                            <i class="fa fa-comments"></i>
                        </div>
                        <div class="value">
                            <h1 class="">Posts</h1>
                            <p>&nbsp;</p>
                        </div>
                    </section>
                </a>
            </div>
            <div class="col-lg-3 col-sm-6">

                <a href="<?= base_url('cms_blog/video') ?>">


                    <section class="panel" >
                        <div class="symbol  red">
                            <i class="fa fa-play"></i>
                        </div>
                        <div class="value">
                            <h1 class="">Edit blog Video</h1>
                            <p>&nbsp;</p>
                        </div>
                    </section>
                </a>
            </div>



        </div>

    </div>
</section>
