<?php



class Cms extends MX_Controller {
   function __construct() {
        parent::__construct();
        $this->upload_path = realpath(APPPATH . "../uploads");
        $this->load->model('administrator_model');
        
    }
    function index() {
        $session_user = $this->session->all_userdata();        
        $is_logged = $this->session->userdata('logged_in');
        if ($is_logged === TRUE) {
            $header_data['user'] = $session_user['username'];
            
            
  /*
   * Begin load of CMS views and Configuration defaults
   */          
            // HTML <HEAD> load config_cms data
            $head['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["head"] = $this->load->view('head', $head, true);            
            //HTML BODY <HEADER> Load data
            $header_data['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["header"] = $this->load->view('header', $header_data, true);
            
            //HTML BODY <ASIDE> Load MainMenu
            $cms_menu = $this->administrator_model->menuIndex();
            $aside_data['cms_menu'] = json_encode($cms_menu);
            
            
            $template_data["aside"] = $this->load->view('aside', $aside_data, true);
            
            //HTML BODY <FOOTER> Load Footer Info
            $template_data["footer"] = $this->load->view('footer', NULL, true);
   /*
    * End of CMS defaults Section.
    */      
            
    /*
     * Here goes all the data section module 
     */ 
            //HTML BODY content #main-content Load Info
            $moduleData['variable'] = '';
           $template_data["mainview"] = $this->load->view('default', $moduleData, true);
            
            
            //other Configurations
            
            $template_data['cms_config'] = $this->administrator_model->cmsConfig();
            $this->load->view('index', $template_data);
           // redirect(base_url('templates/view'));
        } else {
            
            //$datos_plantilla["functions"] = $this->load->view('general_includes/cms_functions', $returns, true);
            redirect(base_url('cms_login'));
        }
    }
    
    
    


}