<?php

/**
 * Administrator_model 
 * 
 * @category Category 
 * @author Diego F. Pineda
 * @email diegopineda@latinmedios.com
 * @agency Latinmedios INC. 
 */
class Administrator_model extends CI_Model {

    var $upload_path;

    function __construct() {
        parent::__construct();
        $this->upload_path = realpath(APPPATH . "../application/uploads");
    }

    /**
     * Validate function
     * @param array $param1 data username and Password of person
     * @return Query Object 
     */
    function validate($data) {
        $this->db->where('username', $data['username']);
        $this->db->where('password', md5($data['password']));
        $query = $this->db->get('tbl_cms_users');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function menuIndex() {
        $this->db->order_by('fk_menu', 'asc');
        $query = $this->db->get('tbl_cms_menu');
        return $query->result();
    }

    function cmsConfig() {
        $this->db->order_by('pk_config', 'asc');
        $query = $this->db->get('tbl_cms_config');
        return $query->result();
    }

}
