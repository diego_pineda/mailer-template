<section>
    <div id="content" class="f-left">   
        <article class="row">
            <div class="sidepost f-left">
                <ul>
<!--                    <li class="like postprop">
                        <a href="#" title="" class="">
                            <span class="posticon">1</span>
                        </a>
                    </li>-->
                    <li class="sep"></li>
                    
                    <li class="comments postprop">
                        <a href="<?=$actual_link?>#comments" class="posticon" title=""></a>
                        <a href="<?=$actual_link?>#comments" title="">0</a>
                    </li>
                    
                    <li class="sep"></li>
                    <? if(isset($article)){ ?>
                    <li class="share postprop">
                        <div class="drop-sharebox">
                            <a href="#" title="" class="posticon">&nbsp;</a>
                            <div class="shareboxin">
                                <a target="_blank" href="http://twitter.com/home?status=<?=$actual_link?>" class="blog-sharer twitter f-left"></a>
                                <a target="_blank" href="http://www.facebook.com/sharer/sharer.php?s=100&amp;p%5Burl%5D=<?=$actual_link?>" class="blog-sharer facebook f-left"></a>
                                <a target="_blank" href="https://plus.google.com/share?url=<?=$actual_link?>" class="blog-sharer gplus f-left"></a>
                                <? if($article){ ?> 
                <? foreach ($article as $k => $v) { ?>
                <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?=$actual_link?>&amp;title=<?= htmlentities($v->headline);?>&amp;source=" class="blog-sharer linkedin f-left"></a>
                                <? } } ?>
                                <div class="socline"></div>
                                <a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());" class="blog-sharer pinterest f-left"></a>
                                <a target="_blank" href="http://digg.com/submit?url=<?=$actual_link?>" class="blog-sharer digg f-left"></a>
                                <a target="_blank" href="http://reddit.com/submit?url=<?=$actual_link?>" class="blog-sharer reddit f-left"></a>
                                <a target="_blank" href="http://www.stumbleupon.com/submit?url=<?=$actual_link?>" class="blog-sharer stumbleupon f-left"></a>
                            </div>
                        </div>
                    </li>
                    <? } ?>
                </ul>
            </div>
            <? if($article){ ?> 
                <? foreach ($article as $k => $v) { ?>
                    <? $images = json_decode($v->gallery)?>
                    <div class="post-content f-left">
                        <div class="carousel slide">
                            <ol class="carousel-indicators">
                                <? if($images){ ?>
                                    <? foreach ($images as $key => $value) { ?>
                                        <li></li>
                                    <? } ?>
                                <? } ?>
                                
                            </ol>
                            <div>
                                <? if($images){ ?>
                                 <ul class="carousel">
                                <? foreach ($images as $key => $value) { ?>
                                    <li class="item <?=  ($key == 0)?'active':''?>">
                                        <img src="<?= base_url(replacePath($value,'full')) ?>"  alt="Nuestra esencia" />
                                    </li>
                                <? } ?>
                                </ul>

                                <? } ?>
                               
                            </div>
                        </div>
                        <h1><a href="#" title="" <?= $category[0]->color ?>><?= $v->name?></a></h1>
                        <div class="post-text">
                            <p><?= $v->headline?></p>
                            <p><?= $v->description?></p>
                        </div>
                        <div class="related-entries">
                            <h4 class="border">Articulos relacionados</h4>
                            <div class="">
                                <? foreach ($articles as $k => $value) { ?>
                                    <? if($value->gallery){?>
                                         <? $images = json_decode($value->gallery)?>
                                        <div class="col-md-4">
                                            <a href="<?= $value->link?>" title=""><img src="<?= base_url(replacePath($images[0],'full')) ?>" width="300px" alt="<?= $value->name?>"></a>
                                            <a href="<?= $value->link?>" class="related-title"><?= $value->name?></a>
                                        </div>
                                    <? } ?>
                                       
                                <? } ?>
                            </div>
                        </div>
                        <? if($comments) { ?>
                             <aside id="comments" class="post-comments col-md-11">
                                <h4 class="border">Comentarios</h4>
                                <ul class="comments-list">  
                                    <? foreach ($comments as $key => $value) { ?>
                                        <? if($value->publish == 1){ ?>
                                        <li class="">
                                            <div class="rows">
                                                <div class="col-md-11">
                                                    <h5><?= $value->name?></h5>
                                                    <div class="comment-text">
                                                        <p><?= $value->comment?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <? } ?>
                                        
                                    <? } ?>
                                    
                                </ul>
                            </aside>
                        <? }else{ ?>

                        <? } ?>

                        <div class="add-comment col-md-11">
                            <h4 class="border">Comentarios</h4>
                            <div class="comment-form col-md-6">
                                <div id="respond" class="comment-respond">
                                    <form action="<?= base_url('landingn/comment')?>" method="post" id="commentform" class="comment-form">
                                        <div class="form-row">
                                            <label for="author">NOMBRE<span class="required">*</span>
                                            </label>
                                            <input id="name" name="name" type="text" class="validate[required]" value="" size="30">
                                        </div>
                                        <div class="form-row">
                                            <label for="email">EMAIL<span class="required">*</span>
                                            </label> 
                                            <input id="email" name="email" type="text" class="validate[required]" value="" size="30">
                                        </div>
                                        <div class="form-row">
                                            <label for="comment">MENSAJE</label>
                                            <textarea id="comment" name="comment" aria-required="true"></textarea>
                                        </div>                                             
                                        <p class="form-submit">
                                            <input name="submit" type="submit" id="submit" value="ENVIAR">
                                            <input type="hidden" name="fk_blog_article" value="<?= $v->pk_blog_article?>">
                                            <input type="hidden" name="pk_blog_comment" value="0">
                                            
                                        </p>
                                        <div id="status"></div>
                                    </form> 
                                </div><!-- #respond -->
                            </div>
                        </div>
                    </div>
                <? } ?>
            <? }?>
           
        </article>
    </div>  
    <aside id="sidebar" class="f-left">
        <ul>
            <li>
                <h2>VISITANOS EN</h2>
                <div class="social">
                    <div class="icon">
                        <a href="#" class="icon-facebook">facebook</a>
                    </div>
                    <div class="icon">
                        <a href="#" class="icon-twitter">twitter</a>
                    </div>
                    <div class="icon">
                        <a href="#" class="icon-google">google</a>
                    </div>
                    <div class="icon">
                        <a href="#" class="icon-youtube">youtube</a>
                    </div>
                    <div class="icon">
                        <a href="#" class="icon-pinterest">pinterest</a>
                    </div>
                    <div class="icon">
                        <a href="#" class="icon-natura">natura</a>
                    </div>
                </div>
            </li>
            <li class="recent-posts-2">
               <li class="recent-posts-2">
                <? 
                    $template_data['articles'] = $articles;
                    $this->load->view('landingn/blocks/recents', $template_data); 
                ?>
            </li> 
                </ul>
            </li>                        <li>
                <h2>VIDEO</h2>
                <?= getVideo($video->url)?>
            </li>
        </ul>                   
    </aside>    
</section>
<script language="javascript">
jQuery(document).ready(function(){
        
            $("#commentform").validationEngine('attach', {
                                onValidationComplete: function(form, status){

                                    if(status){
                                        $('#status').html("Enviando ...");
                                        $.ajax({
                                            type:form.attr('method'),
                                            url: form.attr('action'),
                                            data:form.serialize(),
                                            success:function(res){
                                                var data = jQuery.parseJSON(res);
                                                if(data.status =="ok"){
                                                    $('#status').addClass(data.status);
                                                    $('#status').html(data.msj);
                                                    form.each (function() { this.reset(); });
                                                }else{
                                                    $('#status').addClass(data.status);
                                                    $('#status').html(data.msj);
                                                }

                                            },
                                            error: function(data){
                                                $('#status').html("Intenta nuevamente");
                                            }
                                        });
                                    }else{
                                        return false;
                                    }
                                }
            });
});
</script>
           
