<section>
    <div id="content" class="f-left">   
        <article class="row">
            <div class="sidepost f-left">
                <ul>
<!--                    <li class="like postprop">
                        <a href="#" title="" class="">
                            <span class="posticon">0</span>
                        </a>
                    </li>-->
                    <li class="sep"></li>
<!--                    <li class="comments postprop">
                        <a href="<?=$actual_link?>#comments" class="posticon" title=""></a>
                        <a href="<?=$actual_link?>#comments" title="">0</a>
                    </li>-->
                    <li class="sep"></li>
                      <? if(isset($article)){ ?>
                    <li class="share postprop">
                        <div class="drop-sharebox">
                            <a href="#" title="" class="posticon">&nbsp;</a>
                            <div class="shareboxin">
                                <a target="_blank" href="http://twitter.com/home?status=<?=$actual_link?>" class="blog-sharer twitter f-left"></a>
                                <a target="_blank" href="http://www.facebook.com/sharer/sharer.php?s=100&amp;p%5Burl%5D=<?=$actual_link?>" class="blog-sharer facebook f-left"></a>
                                <a target="_blank" href="https://plus.google.com/share?url=<?=$actual_link?>" class="blog-sharer gplus f-left"></a>
                                <? if($article){ ?> 
                <? foreach ($article as $k => $v) { ?>
                <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?=$actual_link?>&amp;title=<?= htmlentities($v->headline);?>&amp;source=" class="blog-sharer linkedin f-left"></a>
                                <? } } ?>
                                <div class="socline"></div>
                                <a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());" class="blog-sharer pinterest f-left"></a>
                                <a target="_blank" href="http://digg.com/submit?url=<?=$actual_link?>" class="blog-sharer digg f-left"></a>
                                <a target="_blank" href="http://reddit.com/submit?url=<?=$actual_link?>" class="blog-sharer reddit f-left"></a>
                                <a target="_blank" href="http://www.stumbleupon.com/submit?url=<?=$actual_link?>" class="blog-sharer stumbleupon f-left"></a>
                            </div>
                        </div>
                    </li>
                    <? } ?>
                </ul>
            </div>
            <?php if($articles){ ?>
                <?php foreach ($articles as $k => $v) { ?>
                    <?php $images = json_decode($v->gallery)?>
                    <div class="post-content f-left">
                        <div class="carousel slide">
                            <ol class="carousel-indicators">
                                 <?php foreach ($images as $key => $value) { ?>
                                    <li></li>
                                <?php } ?>
                            </ol>
                            <div>
                                <?php if($images){ ?>
                                 <ul class="carousel">
                                <?php foreach ($images as $key => $value) { ?>
                                    <li class="item <?=  ($key == 0)?'active':''?>">
                                        <img src="<?= base_url(replacePath($value,'full')) ?>"  alt="Nuestra esencia" />
                                    </li>
                                <?php } ?>
                                </ul>

                                <?php } ?>
                               
                            </div>
                        </div>
                        <h1><a href="<?= ($v->link)?$v->link:''?>" title="" <?= $category[0]->color ?>><?= $v->name?></a></h1>
                        <div class="post-text">
                            <p><?= $v->headline?></p>
                            <p><a href="<?= ($v->link)?$v->link:''?>" title="Leer mas">Leer mas</a></p>
                        </div>
                    </div>
                <?php } ?>
            <?php }else{?>
                <div class="post-content f-left">
                    No existe contenido, visita otra de nuestas categorias.
                </div>
            <?php } ?>
           
        </article>
    </div>  
    <aside id="sidebar" class="f-left">
        <ul>
            <li>
                <h2>VISITANOS EN</h2>
                <div class="social">
                    <div class="icon">
                        <a href="#" class="icon-facebook">facebook</a>
                    </div>
                    <div class="icon">
                        <a href="#" class="icon-twitter">twitter</a>
                    </div>
                    <div class="icon">
                        <a href="#" class="icon-google">google</a>
                    </div>
                    <div class="icon">
                        <a href="#" class="icon-youtube">youtube</a>
                    </div>
                    <div class="icon">
                        <a href="#" class="icon-pinterest">pinterest</a>
                    </div>
                    <div class="icon">
                        <a href="#" class="icon-natura">natura</a>
                    </div>
                </div>
            </li>
            <li class="recent-posts-2">
                <?php
                    $template_data['articles'] = $articles;
                    $this->load->view('landingn/blocks/recents', $template_data); 
                ?>
            </li>                        
            <li>
                <h2>VIDEO</h2>
                <?= getVideo($video->url)?>
            </li>
        </ul>                   
    </aside>    
</section>
            
           
