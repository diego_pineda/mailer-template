
<? if($articles){ ?>
    <h2>NOTICIAS RECIENTES</h2>
    <ul>
        <? foreach ($articles as $k => $v) { ?>
            <li>
                <a href="<?= $v->link?>" title=""><?= $v->name?></a>
                <span class="post-date"><?= $v->date_added?></span>
            </li>
        <? } ?>
    </ul>
<? } ?>