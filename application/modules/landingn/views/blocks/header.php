<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />

        <!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
        Remove this if you use the .htaccess -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <title>Natura - Blog</title>
        <meta name="description" content="" />
        <meta name="author" content="Mario Bonilla" />

        <meta name="viewport" content="width=device-width; initial-scale=1.0" />

        <!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
        <link rel="shortcut icon" href="/favicon.ico" />
        <link rel="apple-touch-icon" href="/apple-touch-icon.png" />
        
        <!-- CSS -->
        <link href="<?= base_url('landingn/assets/css/core.css') ?>" rel="stylesheet" media="all">
        
        <!--FONT -->
       	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>

        <!-- jquery -->
        <script src="<?= base_url('landingn/assets/js/jquery-1.8.3.min.js') ?>" type="text/javascript" charset="utf-8"></script>


         <!-- validation engine -->
        <link href="<?= base_url('landingn/assets/css/core.css') ?>" rel="stylesheet" media="all">
        <link rel="stylesheet" href="<?= base_url('landingn/assets/validationengine/css/validationEngine.jquery.css') ?>" type="text/css"/>
        <script src="<?= base_url('landingn/assets/validationengine/js/languages/jquery.validationEngine-es.js') ?>" type="text/javascript" charset="utf-8">
        </script>
        <script src="<?= base_url('landingn/assets/validationengine/js/jquery.validationEngine.js') ?>" type="text/javascript" charset="utf-8"></script>


        
    </head>

    <body>
        <div> 
            <header>
                <div id="logo" class="col_logo"> 
                    <a href="<?= base_url('landingn')?>" title="Natura es estar bien"> 
                        <img src="<?= base_url('landingn/assets/img/logo.jpg') ?>" alt=""  />
                    </a>
                </div>
                <nav id="main-top" >
                    <ul>
                    <li><a href="<?= base_url('landingn')?>">Home</a></li>                   
                    <li class="contsubmenu"><a href="javascript:void(0)">Categorias</a>
                        <? if($categories){ ?>
                             <ul class="dropdown-menu">
                                <? foreach($categories as $k => $v){ ?> 
                                    <? if($v->featured == 1 ){ ?>
                                            <li class="item_<?= $k?>"><a href="<?= base_url('/landingn/category/'.$v->alias) ?> " <?= $v->color ?>  title="<?= $v->name ?>"><?= $v->name ?></a></li>
                                        <? } ?>
                                <? }?>
                                <li class="item_f contsubsubmenu"><a href="#" title="OTRAS CATEGORIAS »">OTRAS CATEGORIAS »</a>
                                <ul class="dropdown-submenu">
                                    <? foreach($categories as $k => $v){ ?> 
                                        <? if($v->featured != 1 ){ ?>
                                                <li class="item_<?= $k?>"><a href="<?= base_url('/landingn/category/'.$v->alias) ?> " <?= $v->color ?>  title="<?= $v->name ?>"><?= $v->name ?></a></li>
                                            <? } ?>
                                    <? }?>
                                </ul>
                                </li>
                             </ul>
                       <?  }?>
                    </li>
                    </ul>
                </nav>
                <div class="search-container">
                    <img src="<?= base_url('landingn/assets/img/natura-informa.jpg') ?>" width="163" alt="">
                </div>
            </header>