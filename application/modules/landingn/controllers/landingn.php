<?php

class Landingn extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->upload_path = realpath(APPPATH . "../uploads");
        $this->load->model('cms/administrator_model');
        $this->load->model('cms_blog/cms_blog_model');
        //$this->load->model('cms_login_model');
    }

    public function index() {
        /*
         * Begin load of CMS views and Configuration defaults
         */
        // HTML <HEAD> load config_cms data

        /*
         * End of  data section module 
         */
        $template_data['actual_link'] = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $template_data['categories'] = $this->cms_blog_model->getCategories();
        $template_data['articles'] = $this->cms_blog_model->getArticlesByCategory('importante');
        $template_data['category'] = $this->cms_blog_model->getCategoryByAlias('importante');
        $video = $this->cms_blog_model->getVideo();
        $template_data['video'] = $video[0];

        $this->load->view('landingn/blocks/header', $template_data);
        $this->load->view('landingn/index', $template_data);
        $this->load->view('landingn/blocks/footer', $template_data);
    }

    function category($category='') {
        if ($category) {
            $template_data['actual_link'] = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $template_data['categories'] = $this->cms_blog_model->getCategories();
            $template_data['articles'] = $this->cms_blog_model->getArticlesByCategory($category);
            
            $template_data['category'] = $this->cms_blog_model->getCategoryByAlias($category);
            if($template_data['category']){
            $video = $this->cms_blog_model->getVideo();
            $template_data['video'] = $video[0];
            $this->load->view('landingn/blocks/header', $template_data);
            $this->load->view('landingn/index', $template_data);
            $this->load->view('landingn/blocks/footer', $template_data);
            }else{
              show_404();  
            }
        } else {
            redirect(base_url('landingn'));
        }
    }

    function page($category='', $name='') {
        if ($name) {
            $template_data['actual_link'] = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $template_data['categories'] = $this->cms_blog_model->getCategories();
            $template_data['category'] = $this->cms_blog_model->getCategoryByAlias($category);
            $article = $this->cms_blog_model->getArticlebyAlias($name);
           
            if ($article){
                 $template_data['articles'] = $this->cms_blog_model->getArticlesByCategory($category);
            
            $template_data['comments'] = $this->cms_blog_model->getComments($article[0]->pk_blog_article);
            $template_data['article'] = $article;
            $video = $this->cms_blog_model->getVideo();
            $template_data['video'] = $video[0];

            $this->load->view('landingn/blocks/header', $template_data);
            $this->load->view('landingn/article', $template_data);
            $this->load->view('landingn/blocks/footer', $template_data);
            }else{
               show_404(); 
            }
            
        } else {
            show_404(); 
           redirect(base_url('landingn/'.$category));
        }
    }

    function comment() {
        $post = $this->input->post();
        //$id = $this->helperdb->addCms_blog_comments($post);
        if (true) {
            $data['status'] = 'ok';
            $data['msj'] = 'Su comentarios ha sido Almacenado correctamente.';
        } else {
            $data['status'] = 'error';
            $data['msj'] = 'por favor vuelva a intentarlo';
        }
        echo json_encode($data);
        exit;
    }

}
