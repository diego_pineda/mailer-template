<?php

class Cms_login extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->upload_path = realpath(APPPATH . "../uploads");
        $this->load->model('cms/administrator_model');
    }

    public function index() {
        /*
         * Begin load of CMS views and Configuration defaults
         */
        // HTML <HEAD> load config_cms data
        $head['cms_data'] = $this->administrator_model->cmsConfig();
        $template_data["head"] = $this->load->view('cms/head', $head, true);
        //HTML BODY <HEADER> Load data
        $header_data['cms_data'] = '';
        $template_data["header"] = '';
        //HTML BODY <ASIDE> Load MainMenu
        $cms_menu = '';
        $aside_data['cms_menu'] = '';
        $template_data["aside"] = '';
        //HTML BODY <FOOTER> Load Footer Info
        $template_data["footer"] = $this->load->view('cms/footer', NULL, true);
        //other Configurations
        $template_data['cms_config'] = $this->administrator_model->cmsConfig();
        $moduleData['def_var'] = '';
        /*
         * End of CMS defaults Section.
         */
        /*
         * Here goes all the data section module 
         */
        $template_data["mainview"] = $this->load->view('login_view', $moduleData, true);
        /*
         * End of  data section module 
         */
        $this->load->view('cms/index', $template_data);
    }

    function loginup() {
        $post = $this->input->post();        
        $pass = trim(md5($post['password']));
        $user = $this->db->query('select tbl_cms_users.*, tbl_cms_clients.name as clientname, tbl_cms_clients.logo  from tbl_cms_users left outer join tbl_cms_clients on '
                . 'tbl_cms_users.fk_client=tbl_cms_clients.pk_client'
                . ' where email = "'.$post['email'].'" and password = "'.$pass.'" and tbl_cms_users.fk_status=1');
        $resultado = $user->row_array();        
        if ($user->num_rows() > 0) {
            $newdata = array(
                'username' => $resultado['name'],
                'role' => $resultado['fk_role'],
                'client' => $resultado['fk_client'],
                'logo' => $resultado['logo'],
                'logged_in' => true
            );
            $this->session->set_userdata($newdata);
            $this->lastLogin($resultado['pk_user']);
            redirect('../cms', $newdata);
        } else {
            redirect('../cms');
        }
    }


    function lastLogin($pk_user){
         $this->load->model('cms_users/cms_users_model');
         $arr = array('pk_user' => $pk_user,'last_login'=>date("Y/m/d H:i"));
         $this->cms_users_model->setUser($arr);
    }

    function logout() {
        $this->session->sess_destroy();
        redirect('../cms');
    }

}