<center>
    <section class="panel" style="width: 600px; margin-left: -260px;">  
        <header class="panel-heading">
            Login - base cms
        </header>
        <div class="panel-body">
            <form role="form" class="form-horizontal" method="post" action="cms_login/loginup">
                <div class="form-group">
                    <label class="col-lg-2 col-sm-2 control-label" for="inputEmail1">Email</label>
                    <div class="col-lg-10">
                        <input type="email" name="email" placeholder="Email" id="inputEmail1" class="form-control">                        
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 col-sm-2 control-label" for="inputPassword1">Password</label>
                    <div class="col-lg-10">
                        <input type="password" name="password" placeholder="Password" id="inputPassword1" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                        <button class="btn btn-danger" type="submit">Sign in</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</center>