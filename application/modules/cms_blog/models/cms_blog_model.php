<?php

/**
 * Administrator_model 
 * 
 * @category Category 
 * @author Diego F. Pineda
 * @email diegopineda@latinmedios.com
 * @agency Latinmedios INC. 
 */
class Cms_blog_model extends CI_Model {

    var $upload_path;

    function __construct() {
        parent::__construct();
        $this->upload_path = realpath(APPPATH . "../application/uploads");
        $this->load->model('../cms/models/administrator_model');
    }

    /**
     * Validate function
     * @param array $param1 data username and Password of person
     * @return Query Object 
     */

    function articleList() {
        $this->db->select('*');
        $this->db->from('tbl_cms_blog_articles');
        $this->db->where('tbl_cms_blog_articles.fk_status', '1');
        $query = $this->db->get();
        $result =  $query->result();
        return $this->getLink($result);
        
    }

    function getLink($result){
        if($result){
            foreach ($result as $key => $value) {
                $category =  $this->getCategory($value->fk_blog_category);
                $result[$key]->link = base_url('/landingn/page/'.$category[0]->alias.'/'.$value->alias);
            }
        }
        return $result;
    }


     function getColor($result){
        if($result){
            foreach ($result as $key => $value) {
                $result[$key]->color = 'style="color:'.$value->color.'"';
            }
        }
        return $result;
    }

    function getVideo(){
        $this->db->select('*');
        $this->db->from('tbl_cms_blog_video');
        $this->db->where('tbl_cms_blog_video.fk_status', '1');
        $query = $this->db->get();
        return $query->result();
    }

     function getCategories() {
        $this->db->select('*');
        $this->db->from('tbl_cms_blog_categories');
        $this->db->where('tbl_cms_blog_categories.fk_status', '1');
        $query = $this->db->get();
        return $query->result();
    }

    
    function getArticle($pk_blog_article='') {
        
        if($pk_blog_article!=''){
            $this->db->select('*');
            $this->db->from('tbl_cms_blog_articles');
            $this->db->where('tbl_cms_blog_articles.pk_blog_article', $pk_blog_article);
            $query = $this->db->get();
            $result =  $query->result();
            return $this->getLink($result);

        }else{
            return clearData('tbl_cms_blog_articles'); 
        }
        
    }


     function getArticlesByCategory($alias_blog_category='') {
        
        if($alias_blog_category!=''){
            $this->db->select('tbl_cms_blog_articles.*');
            $this->db->from('tbl_cms_blog_articles');
            $this->db->join('tbl_cms_blog_categories', 'tbl_cms_blog_categories.pk_blog_category = tbl_cms_blog_articles.fk_blog_category', 'inner');
            $this->db->where('tbl_cms_blog_categories.alias', $alias_blog_category);
            //$this->db->where('tbl_cms_blog_articles.pk_blog_article', $pk_blog_article);
            $query = $this->db->get();
            $result =  $query->result();
            return $this->getLink($result);

        }else{
            return clearData('tbl_cms_blog_articles'); 
        }
        
    }

    function getArticlebyAlias($alias_blog_article='') {
        
        if($alias_blog_article!=''){
            $this->db->select('*');
            $this->db->from('tbl_cms_blog_articles');
            $this->db->where('tbl_cms_blog_articles.alias', $alias_blog_article);
            $query = $this->db->get();
            $result = $query->result();
            return $result;
        }else{
            return clearData('tbl_cms_blog_articles'); 
        }
        
    }

    function getCategory($pk_blog_category='') {
        
        if($pk_blog_category!=''){
            $this->db->select('*');
            $this->db->from('tbl_cms_blog_categories');
            $this->db->where('tbl_cms_blog_categories.pk_blog_category', $pk_blog_category);
            $query = $this->db->get();
            return $query->result();

        }else{
            return clearData('tbl_cms_blog_categories'); 
        }
        
    }

     function getCategoryByAlias($alias_blog_category='') {
        
        if($alias_blog_category!=''){
            $this->db->select('*');
            $this->db->from('tbl_cms_blog_categories');
            $this->db->where('tbl_cms_blog_categories.alias', $alias_blog_category);
            $query = $this->db->get();
            return $this->getColor($query->result());

        }else{
            return clearData('tbl_cms_blog_categories'); 
        }
        
    }


    function getComments($pk_blog_article='') {
        
        if($pk_blog_article!=''){
            $this->db->select('*');
            $this->db->from('tbl_cms_blog_comments');
            $this->db->where('tbl_cms_blog_comments.fk_blog_article', $pk_blog_article);
            $query = $this->db->get();
            return $query->result();

        }else{
            return clearData('tbl_cms_blog_comments'); 
        }
        
    }
    
    
    
//
//    function cmsConfig() {
//        $this->db->order_by('pk_config', 'asc');
//        $query = $this->db->get('tbl_cms_config');
//        return $query->result();
//    }

}
