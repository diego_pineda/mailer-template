<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <form id="form-edit" class="form-horizontal form" action="/cms_blog/save/" method="post"  enctype="multipart/form-data">
        <input type="hidden" name='pk_blog_article' value='<?= $data->pk_blog_article?>'>
        <input type="hidden" name='table' value='cms_blog_articles'>
        <input type="hidden" name='redirect' value='cms_blog/edit/'> 
        <header class="panel-heading">
           CMS Post Admin - <?php echo ($data === false || count($data)==0) ? "Add Post":"Edit Post"; ?>
        </header>
        <div>
          <?= html_messages()?> 
        </div>
        <div>
          <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#detail">Detail</a> 
                </li>
                <li class="">
                    <a data-toggle="tab" href="#gallery">Gallery</a>
                </li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <div id="detail" class="tab-pane active">
                      <div class="form-group">
                          <label for="name" class="col-lg-2 col-sm-2 control-label">Name</label>
                          <div class="col-lg-10 ">
                              <input type="text" name="name" class="form-control validate[required]" id="name" placeholder="Enter Name" value="<?= $data->name ?>">
                              <!--<p class="help-block">Complete Name</p>-->
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="name" class="col-lg-2 col-sm-2 control-label">Alias</label>
                          <div class="col-lg-10 ">
                              <input type="text" name="alias" class="form-control" id="alias" placeholder="Enter alias" value="<?= $data->alias?>">
                              <!--<p class="help-block">Complete Name</p>-->
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="name" class="col-lg-2 col-sm-2 control-label">Category</label>
                          <div class="col-lg-10 ">
                              <select name="fk_blog_category" id="fk_blog_category" class="form-control validate[required]">
                                <option value="">Choose option</option>
                                <? if($categories){ ?>
                                    <? foreach ($categories as $key => $value) { ?> 
                                        <? $selected = ($value->pk_blog_category == $data->fk_blog_category)?'selected':''; ?>
                                        <option value="<?= $value->pk_blog_category ?>" <?= $selected?> ><?= $value->name?></option>
                                    <? } ?>
                                <? } ?>
                              </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="name" class="col-lg-2 col-sm-2 control-label">Headline</label>
                          <div class="col-lg-10 ">
                              <textarea class="form-control" rows="5" name="headline"><?= $data->headline?></textarea>
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="name" class="col-lg-2 col-sm-2 control-label">Description</label>
                          <div class="col-lg-10 ">
                              <textarea class="wysihtml5 form-control" rows="10" name="description"><?= $data->description?></textarea>
                          </div>
                      </div>
                </div>
                <div id="gallery" class="tab-pane">
                  <label class="control-label col-md-2">
                    <p><span class="label label-danger">NOTE!</span>
                           <span>
                           Attached image thumbnail is
                           supported in Latest Firefox, Chrome, Opera,
                           Safari and Internet Explorer 10 only
                           </span></p>
                           <a href="javascript:void(0)" class="btn btn-primary add"><i class="fa fa-user"></i> Add Imagen</a>
                   </label>

                  <div class="col-md-9">
                    <? if($data->gallery){ 
                        $gallery = json_decode($data->gallery);
                      ?>
                      <? foreach ($gallery as $key => $value) { ?>

                        <div class="fileupload fileupload-new  col-md-3 upload" id="1" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                  <img src="<?= base_url(replacePath($value,'medium')) ?>" alt="tet 1" />
                                    <!-- <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="tet 1" /> -->
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
                                   <span class="btn btn-white btn-file">
                                     <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                     <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                     <input type="file" class="default" name="file[]" />
                                     <input type="hidden" name="url_file[]" class="url-file" value="<?= $value?>">
                                   </span>
                                   <a href="javascript:void(0)" onclick="deleteRow(this)" class="btn btn-danger"><i class="fa fa-trash"></i> Remove</a>
                                </div>
                            </div>
                        
                      <? } ?>

                   <?  }else{ ?>

                       <div class="fileupload fileupload-new  col-md-3 upload" id="1" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="tet 1" />
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
                                   <span class="btn btn-white btn-file">
                                     <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                     <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                     <input type="file" class="default" name="file[]" />
                                     <input type="hidden" name="url_file[]" class="url-file" value="">
                                   </span>
                                   <a href="javascript:void(0)" onclick="deleteRow(this)" class="btn btn-danger"><i class="fa fa-trash"></i> Remove</a>
                                </div>
                            </div>

                    <? } ?>
                </div>
              </div>
            </div>
            <div class="col-md-12">
                  <button type="submit" class="btn btn-success">Save</button>
                  <a class="btn btn-info" href="<?= base_url('cms_blog')?>">Cancel</a>
            </div>
        </div>
      </form>
    </section>
  </div>
</div>
