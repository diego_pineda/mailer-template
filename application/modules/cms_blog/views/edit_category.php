<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <form class="form-horizontal" action="/cms_blog/save/" method="post"  enctype="multipart/form-data">
        <input type="hidden" name='pk_blog_category' value='<?= $data->pk_blog_category?>'>
        <input type="hidden" name='table' value='cms_blog_categories'>
        <input type="hidden" name='redirect' value='cms_blog/edit_category/'> 
        <header class="panel-heading">
           CMS Post Admin - <?php echo ($data === false || count($data)==0) ? "Add Post":"Edit Post"; ?>
        </header>
        <div>
          <?= html_messages()?> 
        </div>
        <div>
          <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#detail">Detail</a> 
                </li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <div id="detail" class="tab-pane active">
                      <div class="form-group">
                          <label for="name" class="col-lg-2 col-sm-2 control-label">Name</label>
                          <div class="col-lg-10 ">
                              <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name" value="<?= $data->name ?>">
                              <!--<p class="help-block">Complete Name</p>-->
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="name" class="col-lg-2 col-sm-2 control-label">Alias</label>
                          <div class="col-lg-10 ">
                              <input type="text" name="alias" class="form-control" id="alias" placeholder="Enter alias" value="<?= $data->alias?>">
                              <!--<p class="help-block">Complete Name</p>-->
                          </div>
                      </div>
                       <div class="form-group">
                          <label for="name" class="col-lg-2 col-sm-2 control-label">Color</label>
                          <div class="col-lg-10 ">
                              <input type="text" name="color" class="colorpicker-default form-control" id="Color" placeholder="Select color" value="<?= $data->color?>">
                              <!--<p class="help-block">Complete Name</p>-->
                          </div>
                      </div>
                </div>
            </div>
            <div class="col-md-12">
                  <button type="submit" class="btn btn-success">Save</button>
                  <a class="btn btn-info" href="<?= base_url('cms_blog/category_list')?>">Cancel</a>
            </div>
        </div>
      </form>
    </section>
  </div>
</div>
<script>
  jQuery(function(){

    //colorpicker start

    jQuery('.colorpicker-default').colorpicker({
        format: 'hex'
    });

    //wysihtml5 start

    jQuery('.wysihtml5').wysihtml5();

    //wysihtml5 end


     jQuery('.add').on('click',function(e){
      e.preventDefault();
      var container = jQuery('.col-md-9');
      var content = container.find(".upload" ).eq(0);
      var newcontent = content.get(0).cloneNode(true);
      jQuery(newcontent).find('img').eq(0).attr('src','http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image');
      jQuery(newcontent).find('.url-file').val('');
      container.prepend( newcontent); 


    });

  })

  function deleteRow(obj){
    jQuery(obj).parents('.upload').eq(0).remove(); 
    //console.log(parent); 
  }
</script>