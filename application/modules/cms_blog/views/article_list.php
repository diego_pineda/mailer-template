
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Admin Article 
            </header>
            <div class="panel-body">
                <div class="adv-table">
                    <a href="<?php echo base_url('cms_blog/edit/'); ?>" class="btn btn-primary"><i class="fa fa-user"></i> Add Article</a>
                    <table  class="display table table-bordered table-striped" id="data_table">
                        <thead>
                            <tr>
                                <th class="center hidden-phone" style="width: 30px">Id.</th>
                                <th>Title</th>
                                <th>Alias</th>
                                <th>Category</th>
                                <th>Publish</th>
                                <th>Comments</th>
                                <th class="center" style="width: 30px">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($data as $v): ?>
                            <? $publish = ($v->publish)?'<a  class="btn btn-success publish" data-pk="'.$v->pk_blog_article.'" rel="0"><i class="fa  fa-check"></i></a>':'<a  class="btn btn-danger publish" data-pk="'.$v->pk_blog_article.'" rel="1"><i class="fa fa-times"></i></a>'; ?>
                                <tr class="">
                                    <td class="center hidden-phone"><?= $v->pk_blog_article; ?></td>
                                    <td><?= $v->name; ?></td>
                                    <td><?= $v->alias; ?></td>
                                    <th><?= $v->category; ?></th>
                                    <td>
                                        <?= $publish?>
                                    </td>
                                    <td>
                                        <a class="btn btn-warning" data-toggle="modal" href="<?= base_url('cms_blog/comment_list/'.$v->pk_blog_article)?>">
                                  
                                  Comments
                              </a>
                                    </td>
                                    <td >
                                        <div class="btn-group">
                                            <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><i class="fa "></i> Actions <span class="caret"></span></button>
                                            <ul class="dropdown-menu">

                                                <li><a href="<?= base_url('cms_blog/edit/'.$v->pk_blog_article); ?>">Edit</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#" class="delete" data-pk="<?= $v->pk_blog_article ?>">Delete</a></li>
                                            </ul>

                                        </div>
                                    </td>
                                </tr>

                            <?php endforeach; ?>






                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="center hidden-phone" style="width: 30px">Id.</th>
                                <th>Title</th>
                                <th>Alias</th>
                                <th>Publish</th>
                                <th>Comments</th>
                                <th class="center" style="width: 30px">Actions</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>

<script>
    jQuery(function(){
        jQuery('.publish').on('click',function(){
            var value = jQuery(this).attr('rel');
            var pk_data = jQuery(this).data('pk');
            var url = "<?= base_url('cms_blog/publish/') ?>";
            var btn = jQuery(this);
           jQuery.post(url,{ publish: value, pk_blog_article: pk_data, table: 'cms_blog_articles'},function(data){
                if(data.status == 'ok'){
                    if(value == 0){
                        console.log('btn-success');
                        btn.removeClass('btn-success');
                        btn.addClass('btn-danger');
                        btn.attr('rel',1);
                        btn.find('.fa-check').removeClass('fa-check'); 
                        btn.find('.fa').addClass('fa-times');
                        
                    }else{
                        console.log('btn-danger')
                        btn.removeClass('btn-danger'); 
                        btn.addClass('btn-success');
                        btn.attr('rel',0);
                        btn.find('.fa-times').removeClass('fa-times'); 
                        btn.find('.fa').addClass('fa-check');
                       
                    }
                }
            
            },'json');
        })

    jQuery('.delete').on('click',function(e){
            e.preventDefault();
            var pk_data = jQuery(this).data('pk');
            var url = "<?= base_url('cms_blog/delete/') ?>";
            var btn = jQuery(this);
           jQuery.post(url,{ pk_blog_article: pk_data, table: 'cms_blog_articles'},function(data){
                if(data.status == 'ok'){
                    btn.parents('tr').eq(0).remove();
                }
            
            },'json');
        })

    })
</script>