<div class="row">
  <div class="col-lg-12">
    <section class="panel">
      <form id="form-edit" class="form-horizontal form" action="/cms_blog/save/" method="post"  enctype="multipart/form-data">
        <input type="hidden" name='pk_blog_video' value='<?= $data->pk_blog_video?>'>
        <input type="hidden" name='table' value='cms_blog_video'>
        <input type="hidden" name='redirect' value='cms_blog/video/'> 
        <header class="panel-heading">
           CMS Post Admin - <?php echo ($data === false || count($data)==0) ? "Video":"Video"; ?>
        </header>
        <div>
          <?= html_messages()?> 
        </div>
        <div>
          <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" href="#detail">Detail</a> 
                </li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="tab-content">
                <div id="detail" class="tab-pane active">
                      <div class="form-group">
                          <label for="url" class="col-lg-2 col-sm-2 control-label">Url</label>
                          <div class="col-lg-10 ">
                              <input type="text" name="url" class="form-control validate[required]" id="name" placeholder="Enter Url" value="<?= $data->url ?>">
                              <!--<p class="help-block">Complete Name</p>-->
                          </div>
                      </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
                  <button type="submit" class="btn btn-success">Save</button>
                  <a class="btn btn-info" href="<?= base_url('cms_blog')?>">Cancel</a>
            </div>
        </div>
      </form>
    </section>
  </div>
</div>
