<?php

class Cms_blog extends MX_Controller { 

    function __construct() {
        parent::__construct();
        $this->upload_path = realpath(APPPATH . "../uploads");
        $this->load->model('cms/administrator_model');
        $this->load->model('cms_blog_model');
    }

     function index() {
       
       $session_user = $this->session->all_userdata();
        $is_logged = $this->session->userdata('logged_in');
        if ($is_logged === TRUE) {
            $header_data['user'] = $session_user['username'];
            /*
             * Begin load of CMS views and Configuration defaults
             */
            // HTML <HEAD> load config_cms data
            $head['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["head"] = $this->load->view('cms/head', $head, true);
             //HTML BODY <HEADER> Load data
            $header_data['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["header"] = $this->load->view('cms/header', $header_data, true);
            //HTML BODY <ASIDE> Load MainMenu
            $cms_menu = $this->administrator_model->menuIndex();
            $aside_data['cms_menu'] = json_encode($cms_menu);
            $template_data["aside"] = $this->load->view('cms/aside', $aside_data, true);
            //HTML BODY <FOOTER> Load Footer Info
            $template_data["footer"] = $this->load->view('cms/footer', NULL, true);
            //other Configurations
            $template_data['cms_config'] = $this->administrator_model->cmsConfig();
            $moduleData['def_var'] = '';
            /*
             * End of CMS defaults Section.
             */
            /*
             * Here goes all the data section module 
             */
            $template_data["mainview"] = $this->load->view('article_list', $moduleData, true); 

             /***************************************************************************/
            
             /*
              * Here goes all the data section module 
              */ 
                    
                    $this->load->model('cms_blog_model');
                    $data = $this->cms_blog_model->articleList();
                    if($data){
                        foreach ($data as $key => $value) {
                            if($value->fk_blog_category){
                                $category = $this->cms_blog_model->getCategory($value->fk_blog_category);
                                //print_r($category);
                                $data[$key]->category = $category[0]->name;
                            }else{
                                $data[$key]->category = '';
                            }
                        }
                    }
                    $moduleData['data'] = $data;
                    $template_data["mainview"] = $this->load->view('article_list', $moduleData, true);
                    
                    
                    
                     
              /*
              * End of  data section module 
              */ 
              /***************************************************************************/
            /*
             * End of  data section module 
             */
            $this->load->view('cms/index', $template_data);
        } else {
            redirect(base_url('cms_login'));
        }
    }

     function comment_list() {
       
       $session_user = $this->session->all_userdata();
        $is_logged = $this->session->userdata('logged_in');
        if ($is_logged === TRUE) {
            $header_data['user'] = $session_user['username'];
            /*
             * Begin load of CMS views and Configuration defaults
             */
            // HTML <HEAD> load config_cms data
            $head['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["head"] = $this->load->view('cms/head', $head, true);
             //HTML BODY <HEADER> Load data
            $header_data['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["header"] = $this->load->view('cms/header', $header_data, true);
            //HTML BODY <ASIDE> Load MainMenu
            $cms_menu = $this->administrator_model->menuIndex();
            $aside_data['cms_menu'] = json_encode($cms_menu);
            $template_data["aside"] = $this->load->view('cms/aside', $aside_data, true);
            //HTML BODY <FOOTER> Load Footer Info
            $template_data["footer"] = $this->load->view('cms/footer', NULL, true);
            //other Configurations
            $template_data['cms_config'] = $this->administrator_model->cmsConfig();
            $moduleData['def_var'] = '';
            /*
             * End of CMS defaults Section.
             */
            /*
             * Here goes all the data section module 
             */
            $template_data["mainview"] = $this->load->view('comment_list', $moduleData, true); 

             /***************************************************************************/
            
             /*
              * Here goes all the data section module 
              */ 
                    
                    $this->load->model('cms_blog_model');
                    $pk_blog_article=$this->uri->segment(3);
                    $moduleData['data'] = $this->cms_blog_model->getComments($pk_blog_article);
                    $template_data["mainview"] = $this->load->view('comment_list', $moduleData, true);
                    
                    
                    
                     
              /*
              * End of  data section module 
              */ 
              /***************************************************************************/
            /*
             * End of  data section module 
             */
            $this->load->view('cms/index', $template_data);
        } else {
            redirect(base_url('cms_login'));
        }
    }


    function category_list() {
       
       $session_user = $this->session->all_userdata();
        $is_logged = $this->session->userdata('logged_in');
        if ($is_logged === TRUE) {
            $header_data['user'] = $session_user['username'];
            /*
             * Begin load of CMS views and Configuration defaults
             */
            // HTML <HEAD> load config_cms data
            $head['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["head"] = $this->load->view('cms/head', $head, true);
             //HTML BODY <HEADER> Load data
            $header_data['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["header"] = $this->load->view('cms/header', $header_data, true);
            //HTML BODY <ASIDE> Load MainMenu
            $cms_menu = $this->administrator_model->menuIndex();
            $aside_data['cms_menu'] = json_encode($cms_menu);
            $template_data["aside"] = $this->load->view('cms/aside', $aside_data, true);
            //HTML BODY <FOOTER> Load Footer Info
            $template_data["footer"] = $this->load->view('cms/footer', NULL, true);
            //other Configurations
            $template_data['cms_config'] = $this->administrator_model->cmsConfig();
            $moduleData['def_var'] = '';
            /*
             * End of CMS defaults Section.
             */
            /*
             * Here goes all the data section module 
             */
            $template_data["mainview"] = $this->load->view('comment_list', $moduleData, true); 

             /***************************************************************************/
            
             /*
              * Here goes all the data section module 
              */ 
                    
                    $this->load->model('cms_blog_model');
                    $pk_blog_article=$this->uri->segment(3);
                    $moduleData['data'] = $this->cms_blog_model->getCategories();
                    $template_data["mainview"] = $this->load->view('category_list', $moduleData, true);
                    
                    
                    
                     
              /*
              * End of  data section module 
              */ 
              /***************************************************************************/
            /*
             * End of  data section module 
             */
            $this->load->view('cms/index', $template_data);
        } else {
            redirect(base_url('cms_login'));
        }
    }

    function article_list() { 
       $this->index();
    }

    function edit() {
       
        $session_user = $this->session->all_userdata();        
        $is_logged = $this->session->userdata('logged_in');
        if ($is_logged === TRUE) {
            $header_data['user'] = $session_user['username'];
            
            
  /*
   * Begin load of CMS views and Configuration defaults
   */          
            // HTML <HEAD> load config_cms data
            $head['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["head"] = $this->load->view('cms/head', $head, true);
             //HTML BODY <HEADER> Load data
            $header_data['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["header"] = $this->load->view('cms/header', $header_data, true);
            //HTML BODY <ASIDE> Load MainMenu
            $cms_menu = $this->administrator_model->menuIndex();
            $aside_data['cms_menu'] = json_encode($cms_menu);
            $template_data["aside"] = $this->load->view('cms/aside', $aside_data, true);
            //HTML BODY <FOOTER> Load Footer Info
            $template_data["footer"] = $this->load->view('cms/footer', NULL, true);
            //other Configurations
            $template_data['cms_config'] = $this->administrator_model->cmsConfig();
            $moduleData['def_var'] = '';
            
    /*
     * End of CMS defaults Section.
     */ 
            
     /***************************************************************************/
            
     /*
      * Here goes all the data section module 
      */ 
            
            $pk_blog_article=$this->uri->segment(3);
            $this->load->model('cms_blog_model');
            $data = $this->cms_blog_model->getArticle($pk_blog_article);
            $moduleData['data'] = $data[0];
            $categories = $this->cms_blog_model->getCategories();
            $moduleData['categories'] = $categories;
            $template_data["mainview"] = $this->load->view('edit', $moduleData, true);
            
            
            
             
      /*
      * End of  data section module 
      */ 
      /***************************************************************************/
         //HTML BODY content #main-content Load Info   
         $this->load->view('cms/index', $template_data);
        } else {
            redirect(base_url('cms_login'));
        }
    }


     function video() {
       
        $session_user = $this->session->all_userdata();        
        $is_logged = $this->session->userdata('logged_in');
        if ($is_logged === TRUE) {
            $header_data['user'] = $session_user['username'];
            
            
  /*
   * Begin load of CMS views and Configuration defaults
   */          
            // HTML <HEAD> load config_cms data
            $head['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["head"] = $this->load->view('cms/head', $head, true);
             //HTML BODY <HEADER> Load data
            $header_data['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["header"] = $this->load->view('cms/header', $header_data, true);
            //HTML BODY <ASIDE> Load MainMenu
            $cms_menu = $this->administrator_model->menuIndex();
            $aside_data['cms_menu'] = json_encode($cms_menu);
            $template_data["aside"] = $this->load->view('cms/aside', $aside_data, true);
            //HTML BODY <FOOTER> Load Footer Info
            $template_data["footer"] = $this->load->view('cms/footer', NULL, true);
            //other Configurations
            $template_data['cms_config'] = $this->administrator_model->cmsConfig();
            $moduleData['def_var'] = '';
            
    /*
     * End of CMS defaults Section.
     */ 
            
     /***************************************************************************/
            
     /*
      * Here goes all the data section module 
      */ 
            
            $pk_blog_article=$this->uri->segment(3);
            $this->load->model('cms_blog_model');
            $data = $this->cms_blog_model->getVideo();
            $moduleData['data'] = $data[0];
            $template_data["mainview"] = $this->load->view('video', $moduleData, true);
            
            
            
             
      /*
      * End of  data section module 
      */ 
      /***************************************************************************/
         //HTML BODY content #main-content Load Info   
         $this->load->view('cms/index', $template_data);
        } else {
            redirect(base_url('cms_login'));
        }
    }


     function edit_category() {
       
        $session_user = $this->session->all_userdata();        
        $is_logged = $this->session->userdata('logged_in');
        if ($is_logged === TRUE) {
            $header_data['user'] = $session_user['username'];
            
            
  /*
   * Begin load of CMS views and Configuration defaults
   */          
            // HTML <HEAD> load config_cms data
            $head['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["head"] = $this->load->view('cms/head', $head, true);
             //HTML BODY <HEADER> Load data
            $header_data['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["header"] = $this->load->view('cms/header', $header_data, true);
            //HTML BODY <ASIDE> Load MainMenu
            $cms_menu = $this->administrator_model->menuIndex();
            $aside_data['cms_menu'] = json_encode($cms_menu);
            $template_data["aside"] = $this->load->view('cms/aside', $aside_data, true);
            //HTML BODY <FOOTER> Load Footer Info
            $template_data["footer"] = $this->load->view('cms/footer', NULL, true);
            //other Configurations
            $template_data['cms_config'] = $this->administrator_model->cmsConfig();
            $moduleData['def_var'] = '';
            
    /*
     * End of CMS defaults Section.
     */ 
            
     /***************************************************************************/
            
     /*
      * Here goes all the data section module 
      */ 
            
            $pk_blog_category=$this->uri->segment(3);
            $this->load->model('cms_blog_model');
            $data = $this->cms_blog_model->getCategory($pk_blog_category);
            $moduleData['data'] = $data[0];;
            $template_data["mainview"] = $this->load->view('edit_category', $moduleData, true); 
            
            
            
             
      /*
      * End of  data section module 
      */ 
      /***************************************************************************/
         //HTML BODY content #main-content Load Info   
         $this->load->view('cms/index', $template_data);
        } else {
            redirect(base_url('cms_login'));
        }
    }

    function publish(){
      $post = $this->input->post();
      $table = 'add'.$post['table'] ;
      $id = $this->helperdb->$table($post);

      if($id){
        $data['status'] = 'ok';
      }

      echo json_encode($data);
      exit;

    }


}