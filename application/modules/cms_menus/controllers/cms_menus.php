<?php



class Cms_menus extends MX_Controller {
   function __construct() {
        parent::__construct();
        $this->upload_path = realpath(APPPATH . "../uploads");
        $this->load->model('cms/administrator_model');
    }
    function index() {
       
        $session_user = $this->session->all_userdata();        
        $is_logged = $this->session->userdata('logged_in');
        if ($is_logged === TRUE) {
            $header_data['user'] = $session_user['username'];
            
            
  /*
   * Begin load of CMS views and Configuration defaults
   */          
            // HTML <HEAD> load config_cms data
            $head['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["head"] = $this->load->view('cms/head', $head, true);
             //HTML BODY <HEADER> Load data
            $header_data['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["header"] = $this->load->view('cms/header', $header_data, true);
            //HTML BODY <ASIDE> Load MainMenu
            $cms_menu = $this->administrator_model->menuIndex();
            $aside_data['cms_menu'] = json_encode($cms_menu);
            $template_data["aside"] = $this->load->view('cms/aside', $aside_data, true);
            //HTML BODY <FOOTER> Load Footer Info
            $template_data["footer"] = $this->load->view('cms/footer', NULL, true);
            //other Configurations
            $template_data['cms_config'] = $this->administrator_model->cmsConfig();
            $moduleData['def_var'] = '';
    /*
     * End of CMS defaults Section.
     */ 
            
     /***************************************************************************/
            
     /*
      * Here goes all the data section module 
      */ 
            
            $this->load->model('cms_menus_model');
            $moduleData['data'] = $this->cms_menus_model->menuList();
           $template_data["mainview"] = $this->load->view('menu_list', $moduleData, true);
            
            
            
             
      /*
      * End of  data section module 
      */ 
      /***************************************************************************/
         //HTML BODY content #main-content Load Info   
         $this->load->view('cms/index', $template_data);
        } else {
            redirect(base_url('cms_login'));
        }
    }
    
    function edit() {
       
        $session_user = $this->session->all_userdata();        
        $is_logged = $this->session->userdata('logged_in');
        if ($is_logged === TRUE) {
            $header_data['user'] = $session_user['username'];
            
            
  /*
   * Begin load of CMS views and Configuration defaults
   */          
            // HTML <HEAD> load config_cms data
            $head['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["head"] = $this->load->view('cms/head', $head, true);
             //HTML BODY <HEADER> Load data
            $header_data['cms_data'] = $this->administrator_model->cmsConfig();
            $template_data["header"] = $this->load->view('cms/header', $header_data, true);
            //HTML BODY <ASIDE> Load MainMenu
            $cms_menu = $this->administrator_model->menuIndex();
            $aside_data['cms_menu'] = json_encode($cms_menu);
            $template_data["aside"] = $this->load->view('cms/aside', $aside_data, true);
            //HTML BODY <FOOTER> Load Footer Info
            $template_data["footer"] = $this->load->view('cms/footer', NULL, true);
            //other Configurations
            $template_data['cms_config'] = $this->administrator_model->cmsConfig();
            $moduleData['def_var'] = '';
            
    /*
     * End of CMS defaults Section.
     */ 
            
     /***************************************************************************/
            
     /*
      * Here goes all the data section module 
      */ 
            
            $pk_menu=$this->uri->segment(3);
            $this->load->model('cms_menus_model');
            $data = $this->cms_menus_model->getMenu($pk_menu);
            $moduleData['data'] = $data[0];
            $template_data["mainview"] = $this->load->view('edit', $moduleData, true);
            
            
            
             
      /*
      * End of  data section module 
      */ 
      /***************************************************************************/
         //HTML BODY content #main-content Load Info   
         $this->load->view('cms/index', $template_data);
        } else {
            redirect(base_url('cms_login'));
        }
    }

    function save(){
      $post = $this->input->get();
      print_r($post);
    }
    
    


}