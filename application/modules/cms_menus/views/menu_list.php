
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                CMS Menus Admin
            </header>
            <div class="panel-body">
                <div class="adv-table">
                    <a href="<?= base_url('cms_menus/edit/'); ?>" class="btn btn-primary"><i class="fa fa-user"></i> Add menu</a>
                    <table  class="display table table-bordered table-striped" id="data_table">
                        <thead>
                            <tr>
                                <th class="center" style="width: 30px">Id.</th>
                                <th>Name</th>

                                <th class="center" style="width: 30px">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php foreach($data as $item):  ?>
                            <tr class="">
                                <td class="center hidden-phone"><?= $item->pk_menu; ?></td>
                                <td><?=$item->name; ?></td>
                                <td >
                                    <div class="btn-group">
                                        <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><i class="fa "></i> Actions <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            
                                            <li><a href="<?= base_url('cms_menus/edit/'.$item->pk_menu); ?>">Edit</a></li>
                                            <li><a href="#">Inactivate</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">Delete</a></li>
                                        </ul>
                                        
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="center" style="width: 30px">Id.</th>
                                <th>Name</th>

                                <th class="center" style="width: 30px">Actions</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>