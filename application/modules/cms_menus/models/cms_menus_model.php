<?php

/**
 * Administrator_model 
 * 
 * @category Category 
 * @author Diego F. Pineda
 * @email diegopineda@latinmedios.com
 * @agency Latinmedios INC. 
 */
class Cms_menus_model extends CI_Model {

    var $upload_path;

    function __construct() {
        parent::__construct();
        $this->upload_path = realpath(APPPATH . "../application/uploads");
        $this->load->model('../cms/models/administrator_model');
    }

    /**
     * Validate function
     * @param array $param1 data username and Password of person
     * @return Query Object 
     */

    function menuList() {
        
       
       
        $query = $this->db->get("tbl_cms_menu");
        return $query->result();
    }
    
    function getMenu($pk_menu='') {
        
        if($pk_menu!=''){
            $this->db->select('*');
            $this->db->from('tbl_cms_menu');
            $this->db->where('tbl_cms_menu.pk_menu', $pk_menu);
            $query = $this->db->get();
            return $query->result();
        
        }else{
            return false;
        }
        
    }
    
    
    
//
//    function cmsConfig() {
//        $this->db->order_by('pk_config', 'asc');
//        $query = $this->db->get('tbl_cms_config');
//        return $query->result();
//    }

}
