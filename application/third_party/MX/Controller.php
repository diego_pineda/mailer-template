<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/** load the CI class for Modular Extensions **/
require dirname(__FILE__).'/Base.php';

/**
 * Modular Extensions - HMVC
 *
 * Adapted from the CodeIgniter Core Classes
 * @link	http://codeigniter.com
 *
 * Description:
 * This library replaces the CodeIgniter Controller class
 * and adds features allowing use of modules and the HMVC design pattern.
 *
 * Install this file as application/third_party/MX/Controller.php
 *
 * @copyright	Copyright (c) 2011 Wiredesignz
 * @version 	5.4
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/
class MX_Controller 
{
	public $autoload = array();
	
	public function __construct() 
	{
		$class = str_replace(CI::$APP->config->item('controller_suffix'), '', get_class($this));
		log_message('debug', $class." MX_Controller Initialized");
		Modules::$registry[strtolower($class)] = $this;	
		
		/* copy a loader instance and initialize */
		$this->load = clone load_class('Loader');
		$this->load->initialize($this);	
		
		/* autoload module items */
		$this->load->_autoloader($this->autoload);
	}
	
	public function __get($class) {
		return CI::$APP->$class;
	}

	public function save(){
		$this->load->library('UploadFiles'); 
		$post = $this->input->post();
		$data = array();
		$post['alias'] = (!empty($post['alias']))?stringURLSafe($post['alias']):stringURLSafe($post['name']); 

		$table = 'add'.$post['table'];
		unset($post['table']);		

		if(!empty($_FILES)){
			$arr = convertDataFiles($_FILES);
			if($post['url_file']){
				foreach($post['url_file'] as $key => $value) {
					if(!empty($arr[$key])){
						$res = $this->uploadfiles->resizedImages($arr[$key]);
						if($res['status'] == 'ok'){
							$data[$key] = $res['url'];
						}
					}else if(!empty($value)){
						$data[$key] = $value;
					}
				}
			}	
			if(!empty($data)){
				$post['gallery']  = json_encode($data);
			}else{
				$post['gallery']  = '';
			}
			
		}
		
		$post = $this->rules($post);

  		$id = $this->helperdb->$table($post);
		if($id){
			$msg = '<strong>Done!</strong> has been successfully saved';
			$class = 'success';
		}else{
			$msg = '<strong>Error!</strong> error saved data';
			$class = 'danger';
		}
		set_message($class, $msg );
		redirect(base_url($post['redirect'].$id));
	}

	function rules($post){
		return $post;
	}

	 function delete(){ 
      $post = $this->input->post();
      $table = 'add'.$post['table'] ;
      $post['fk_status'] = 0;
      $id = $this->helperdb->$table($post);

      if($id){
        $data['status'] = 'ok';
      }
      if(array_key_exists('redirect',$post)){
      	if($post['redirect']){
      		redirect(base_url($post['redirect']));
      	}
      }else{
      	echo json_encode($data);
      	exit;
      }

      

    } 
}