var _controlSave = 0;
// limit characters
$('.editable-field').click(function() {
    var classList = $(this).attr('class').split(/\s+/);
    //console.log(classList);
    var maxChar = 0;

    $.each(classList, function(index, item) {

        if (item.indexOf('maxchar') != -1) {
            item = item.substring(8);
            item = item.substring(item.length - 1, -1);
            maxChar = item;
        }
    });
    $(this).attr('contenteditable', 'true');


    $(this).keypress(function(e) {
        var actChar = $(this).html().length;
        if (maxChar > 0) {
            if (actChar > maxChar) {
                e.preventDefault();

            } else {
                //console.log("maxChar:" + maxChar);
                //  console.log("actChar:" + actChar);
            }
        } else {
            console.log("maxChar:" + maxChar);
            console.log("actChar:" + actChar);
        }

    });


});



$(".edit_image").click(function() {
    loadImg(this);
});
$(".getTitle").click(function() {
    $(this).attr('data-toggle', "modal");
    $(this).attr('href', "#selectTitle");
    var iniTitle = $(this);
    var srcini = $(this).attr('src');

    var htmlstring = '<div class="modal-content">'
            + '<div class="modal-header">'
            + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'
            + '<h4 class="modal-title">View & save the Source code Template:</h4>'
            + '</div>'
            + '<div class="modal-body" style="height: 500px;" >'
            + '<div class="form-group">'
            + '<label for="Link" class="col-lg-3 col-sm-2 control-label">Select a Title:</label>'

            + '<div id="dataTitles" style="overflow: auto; height: 450px; width: 390px"></div>'
            + '</div>'
            + '</div>'
            + '<div class="modal-footer">'
            + '<button data-dismiss="modal" id="btnCancel" class="btn btn-default" type="button">close</button>'
            + '<button data-dismiss="modal" class="btn btn-success" id="copyCode" type="button" >accept</button>'
            + '</div>'
            + '</div>';
    $('#contentTitles').html(htmlstring);


    var url = document.URL.split('/create');
    url = url[0] + '/loadtitles' + url[1];
    $("#dataTitles").load(url, function() {
        $('img.title').click(function() {
            // console.log(iniTitle);
            $('img.title').css({width: '320px'});
            iniTitle.attr('src', $(this).attr('src'));
            $(this).css({width: '368px'});
            // console.log('clickout');
        });
    });
    $('#btnCancel').click(function() {
        iniTitle.attr('src', srcini);
    })



});


function loadImg(Element) {
    var href = $(Element).attr('src');
    var elementID = $(Element).attr('id');

    var htmlstring = '<div class="modal-header">'
            + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'
            + '<h4 class="modal-title">Upload an Image</h4>'
            + '</div>'
            + '<div class="modal-body" style="height: 430px;">'
            + '<div class="col-md-9" >'
            + '<div class="fileupload fileupload-new  col-md-3 upload" id="1" data-provides="fileupload">'
            + ' <div class="fileupload-new thumbnail" style="width: 500px; height: auto;">'
            + '<img id="loadedImage" src="' + href + '" alt="" />'
            + ' <!-- <img src="http://www.placehold.it/200x1/EFEFEF/AAAAAA&amp;text=load+image" alt="tet 1" /> -->'
            + ' </div>'
            + '<div class="fileupload-preview fileupload-exists thumbnail" style="width: 500px; line-height: 20px;"></div>'
            + ' <div>'
            + ' <span class="btn btn-white btn-file">'
            + '<span class="fileupload-new"><i class="fa fa-paper-clip"></i> select image</span>'
            + '<span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>'
            + ' <input type="file" class="default" name="file[]" />'
            + ' <input type="hidden" name="url_file[]" class="url-file" value="">'
            + '</span>'
            + '<a href="javascript:void(0)" id="deleteImg" class="btn btn-danger">remove</a>'
            + '</div>'
            + '</div>'
            + '</div>'
            + '</div>'
            + '<div class="modal-footer">'
            + '<button data-dismiss="modal" class="btn btn-default" type="button">close</button>'
            + '<button data-dismiss="modal" class="btn btn-success" type="button" onclick="loadImage(\'' + elementID + '\')" >load image</button>'
            + '</div>';

    $('#contentLoadImg').html(htmlstring);

    $("#deleteImg").click(function() {
        var element = '<img src="nothing" />'
        $('#loadedImage').remove();
        $('.fileupload-preview').append(element);
    });

}




$('#template_editable a').click(function(e) {
    e.preventDefault();
    var element = $(this);
    element.attr("data-toggle", "modal");
    var oldHref = element.attr("href");
    var oldText = element.html();

    $(this).attr("href", "#insertLink");

    var htmlstring = '<div class="modal-content">'
            + '<div class="modal-header">'
            + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'
            + '<h4 class="modal-title">Edit Link:</h4>'
            + '</div>'
            + '<div class="modal-body" >'
            + '<div class="form-group">'
            + '<label for="Text" class="col-lg-3 col-sm-2 control-label">Enter a Label Text:</label>';
    if (oldText.indexOf('<img') != -1) {
        htmlstring += '<input type="text" name="Text" class="form-control" id="text" disbled placeholder="N/A. It\'s an icon" value="" disabled>';
    } else {
        htmlstring += '<input type="text" name="Text" class="form-control" id="text" placeholder="" value="' + oldText + '">';
    }
    htmlstring += '</div>'
            + '<div class="form-group">'
            + '<label for="Link" class="col-lg-3 col-sm-2 control-label">Enter a URL:</label>';
    if (oldHref.indexOf('{clickAqui}') != -1) {
        htmlstring += '<input type="Text" name="Link" class="form-control" id="Link" placeholder="this link is not Editable"  disabled>';

    } else {
        htmlstring += '<input type="Text" name="Link" class="form-control" id="Link" placeholder="" value="' + oldHref + '">';
    }
    htmlstring += '</div>'
            + '</div>'
            + '<div class="modal-footer">'
            + '<button data-dismiss="modal" class="btn btn-default" type="button">close</button>'
            + '<button data-dismiss="modal" class="btn btn-success" id="loadLink" type="button" >write link</button>'
            + '</div>'
            + '</div>';
    $('#contentloadLink').html(htmlstring);

    $("#Link").blur(function() {
        var linktext = $(this).val();
        if (/\s/g.test(linktext)) {
            alert("the URL can't contain spaces!")
            $(this).focus();
        }
    });

    $("#loadLink").click(function() {

        var obj = $("#contentloadLink input");
        var text = obj[0].value;
        var link = obj[1].value;


        if (link.indexOf('http://') == -1 && link.indexOf('https://') == -1) {

            link = 'http://' + link;


        }
        if (oldText.indexOf('<img') == -1) {
            element.html(text);
        }
        if (oldHref.indexOf('{clickAqui}') == -1) {
            element.attr("href", link);
        } else {
            element.attr("href", oldHref);
        }
        element.attr("title", link);

    });


});


$("#btnViewSource").click(function() {

    var sourceCode = $("#template_editable").html();





    $('#source').html(sourceCode);
    $('#source').click(function() {
        $('#source').select();
        $('#source').focus();
    });




});

$("#btnSavePrompt").click(function() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;//January is 0! 
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var template_name = 'template_' + dd + '-' + mm + '-' + yyyy;
    var htmlstring = '<div class="form-group">'
            + '<label for="template_name" class="col-lg-3 col-sm-2 control-label"></label>'
            + '<input type="text" name="template_name" class="form-control" id="template_name" placeholder="" value="' + template_name + '">'
            + '</div>'
    $("#prompt_content").html(htmlstring);
});

$("#btnSaveTemplate").click(function() {
    if (_controlSave == 0) {


        var data = {};
       
        data['rawCode'] =$("#template_editable").html();
        var url = document.URL.split('/create');
        url = url[0] + '/savetemplate';
        var bg = $("#mail_" + $("#layout").val()).css('background-color');
        $("#mail_" + $("#layout").val()).attr('style', 'background-color:' + bg + '; margin:0 auto;');
        $('.delete-module').remove();

        $.each($("#template_editable font"), function(index, item) {


            var mainStyle = $(item).attr('style');
            

            $.each($($(item)).children("div"), function(indice, objeto) {
                $(objeto).attr('style', mainStyle);
                $(objeto).css({margin:"0 0 0 0"});
            });


        });



        var sourceCode = $("#template_editable").html();
        data['sourceCode'] = sourceCode;
        data['name'] = $("#template_name").val();
        data['fk_templates_layout'] = $("#layout").val();
        //console.log(data);
        //return false;
        $.ajax({
            type: "POST",
            url: url,
            data: data
        }).done(function(response) {

            $("#template_editable").html(response);

            $("#btnSavePrompt").hide();
            $("#module_load").hide();
            $("#btnViewSource").show();


        });
        _controlSave++;
    }

});



$('.default_mod').mouseover(function() {
    var element = $(this);
    //console.log(element);


    $(this).children('.delete-module').show();

    $(this).children('.delete-module').click(function() {
        $(this.parentNode).remove();
        //$(element).remove();

    });


});
$('.default_mod').mouseleave(function() {
    $('.delete-module').hide();

});






